Fithardware::Application.routes.draw do
  root to: "computers#index"
  get "guides", to: "pages#guides"
  get "about", to: "pages#about"

  devise_for :users

  resources :hulls,
            :power_supplies,
            :rams,
            :hdds,
            :ssds,
            :optical_drives,
            :motherboards,
            :cpus,
            :cpu_coolers,
            :video_cards do
    collection do
      post :filter
      get :filter
    end
  end


  resources :users do
    resources :computers
  end

  resources :computers do
    resources :hulls,
              :power_supplies,
              :rams,
              :hdds,
              :ssds,
              :optical_drives,
              :motherboards,
              :cpus,
              :cpu_coolers,
              :video_cards,
              only: [:index, :filter] do

      member do
        post :add_to
        post :remove_from
      end
      collection do
        post :filter
        get  :filter
      end
    end

    member do
      post :publish
    end
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
