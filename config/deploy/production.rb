server 'ec2-54-210-111-107.compute-1.amazonaws.com',
  user: 'ubuntu',
  roles: %w{web app db},
  ssh_options: {
    keys: %w(~/.ssh/fithardware.pem),
    forward_agent: false,
    auth_methods: %w(publickey password)
  # password: 'please use keys'
  }
