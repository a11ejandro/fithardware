# config valid only for Capistrano 3.1
lock '3.2.1'
require 'capistrano/rvm'

set :application, 'fithardware'
set :repo_url, 'git@bitbucket.org:a11ejandro/fithardware.git'
set :user, 'ubuntu'
set :branch, 'master'
set :deploy_via, :copy
set :use_sudo, false

set :deploy_to, "/var/www/apps/fithardware"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
#set :linked_files, %w[config/mongoid.yml lib/amazon_api.rb]

# Default value for linked_dirs is []
#set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do
  desc 'Restart application'
    task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'unicorn:restart'
    end
  end

  desc 'Setup'
  task :setup do
    on roles(:all) do
      execute "mkdir -p #{shared_path}/config/"
      execute "mkdir -p #{shared_path}/lib/"
      execute "mkdir -p #{deploy_to}/run/"
      execute "mkdir -p #{shared_path}/log/"
      execute "mkdir -p #{deploy_to}/socket/"
      execute "mkdir -p #{deploy_to}/uploads/"

      upload!('config/mongoid.yml', "#{shared_path}/config/mongoid.yml")
      upload!('lib/amazon_api.rb', "#{shared_path}/lib/amazon_api.rb")
      upload!('config/initializers/rekaptcha.rb', "#{shared_path}/config/rekaptcha.rb")
    end
  end

  desc 'Create symlink'
  task :symlink do
    on roles(:all) do
      execute "ln -s #{shared_path}/config/mongoid.yml #{release_path}/config/mongoid.yml"
      execute "ln -s #{shared_path}/config/recaptcha.rb #{release_path}/config/initializers/recaptcha.rb"
      execute "ln -s #{shared_path}/lib/amazon_api.rb #{release_path}/lib/amazon_api.rb"
      execute "ln -s #{deploy_to}/uploads #{release_path}/public/uploads"
      execute "ln -s #{release_path} #{current_path}"
    end
  end

  after :updating, 'deploy:symlink'
  after :publishing, :restart
  after :finishing, 'deploy:cleanup'

  before :setup, 'deploy:starting'
  before :setup, 'deploy:updating'
  before :setup, 'bundler:install'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end
