def login_as_user(user=nil)
  @user = user
  @user ||= (create :user)
  visit new_user_session_path
  fill_in "user_email", with: @user.email
  fill_in "user_password", with: "password"
  click_button "Sign in"
end

def login_as_admin(admin=nil)
  @admin = admin
  @admin ||= (create :user, :admin)
  visit new_user_session_path
  fill_in "user_email", with: @admin.email
  fill_in "user_password", with: "password"
  click_button "Sign in"
end

def fill_in_common_fields(hardware_name)
  fill_in "#{hardware_name}_amazon_price", with: 100500
  fill_in "#{hardware_name}_manufacturer", with: 'Legio Cybernetica'
  fill_in "#{hardware_name}_power_consumption", with: 9000
  fill_in "#{hardware_name}_product_name", with: "#{hardware_name} MK23301"
  fill_in "#{hardware_name}_noise_level", with: 200
  fill_in "#{hardware_name}_description", with: 'Adeptus Mechanicus testing model MK 23301'
end

def create_published_computer(user = nil)
  [:cpu, :cpu_cooler, :motherboard, :ram, :video_card, :hull, :power_supply].each do |hardware_symbol|
    create hardware_symbol, :fitting
  end

  create :hdd
  create :ssd
  create :optical_drive

  computer = create :computer, user: user
  visit edit_computer_path(computer)

  ['Motherboard', 'CPU', 'CPU Cooler', 'RAM', 'HDD', 'SSD', 'Optical Drive', 'Video Card', 'Power Supply', 'Case'].each do |hardware_name|
    add_hardware hardware_name
  end

  click_on 'Verify'

  return computer
end

def add_hardware(hardware_name)
  click_on hardware_name
  click_on 'Add'
end

def initialize_ym_parsing(ym_uri, testing_class)
  FakeWeb.register_uri(:get, ym_uri, :body => File.open("test/fixtures/ym_sample_#{testing_class}.html"), content_type: 'text/html')
  fill_in "#{testing_class}_yandex_market_link", with: ym_uri
  click_on 'Fetch'
end