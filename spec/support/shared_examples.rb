shared_examples 'orderable list' do |hardware_symbol|
  Given do
    @cheap = create hardware_symbol, amazon_price: 100
    @middle_class = create hardware_symbol, amazon_price: 300
    @expensive = create hardware_symbol, amazon_price: 500
    @one_more = create hardware_symbol, amazon_price: 400
  end
  Then do
    within '#order-by' do
      click_on 'Price'
    end
    first('.price').should have_text @cheap.amazon_price.to_s
    within '#order-by' do
      click_on 'Price'
    end
    first('.price').should have_text @expensive.amazon_price.to_s
  end
end

shared_examples 'absent items' do
  Then do
    page.should have_content 'match filter criteria'
  end
end

shared_examples 'showable guide' do
  Then do
    page.should have_css('#guide', visible: false)
    page.find('#toggle-guide').click
    page.should have_css('#guide', visible: true)
  end
end

shared_examples 'amazon fetchable' do |hardware_name|
  When do
    fill_in "#{hardware_name}_amazon_asin", with: 'B005LDLV6S'
    click_on 'Fetch'
  end

  Then do
    find_field("#{hardware_name}[manufacturer]").value.should eq 'Crucial'
    find_field('Product name').value.should eq 'CT51264BF160B'
    find_field('Amazon link').value.should_not be_nil
    find_field('Price').value.should_not be_nil

    find_field("#{hardware_name}[packed_weight]").value.should eq '0.01'
    find_field("#{hardware_name}[packed_height]").value.should eq '12.7'
    find_field("#{hardware_name}[packed_length]").value.should eq '177.8'
    find_field("#{hardware_name}[packed_width]").value.should eq '127'

    find_field("#{hardware_name}[unpacked_weight]").value.should eq '0.01'
    find_field("#{hardware_name}[unpacked_height]").value.should eq '127'
    find_field("#{hardware_name}[unpacked_length]").value.should eq '177.8'
    find_field("#{hardware_name}[unpacked_width]").value.should eq '11.18'
  end
end

shared_examples 'addable hardware' do
  Then do
    visit edit_computer_path(@computer)
    within "##{@hardware.class.name.tableize}" do
      find('.btn').click
    end
    click_on 'Add'
    page.should have_content @hardware.manufacturer
    click_on 'Remove'
    page.should_not have_content @hardware.manufacturer
  end
end

shared_examples 'unfittable computer' do
  When { @computer.publish }
  Then { @computer.published?.should eq false }
end