FactoryGirl.define do
  factory :cpu_cooler, class: CpuCooler, parent: :hardware do
      sockets {[Faker::Lorem.word]}
      min_rotation_speed { rand(600..800) }
      max_rotation_speed{ rand(900..2000)}
      form_factor { CpuCooler::FORM_FACTORS.sample }

      trait :fitting do
        sockets ['Fitting Socket']
        unpacked_height 120
        form_factor 'Tower'
      end
    end
end