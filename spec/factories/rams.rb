# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ram, class: Ram, parent: :hardware do
    generation Ram::GENERATIONS.sample
    capacity { rand(2048..16384) }
    frequency { rand(1666..2444) }
    hackintosh_recommended true
    power_consumption  3

    trait :fitting do
      generation "DDR3"
      capacity 4096
      frequency 1333
    end
  end
end
