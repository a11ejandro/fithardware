# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :optical_drive, class: OpticalDrive, parent: :hardware do
    cd_r_write { rand(30..52) }
    cd_rw_write { rand(10..52) }
    dvd_r_write { rand(10..52) }
    dvd_rw_write { rand(4..52) }
    dvd_dl_write { rand(4..32) }
    bd_r_write { rand(4..32) }
    bd_dl_write { rand(4..16) }
    dvd_read { rand(36..48) }
    bd_read { rand(18..36) }
  end
end
