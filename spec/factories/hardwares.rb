# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hardware do
    amazon_price { rand(30..600) }
    manufacturer { Faker::Company.name }
    product_name { Faker::Lorem.words(2).join(' ') }
    description  { Faker::Lorem.sentences(sentence_count = 4) }
    noise_level  { [0..60].sample }
    hackintosh_recommended [true, false].sample

    trait :hackintosh_recommended do
      hackintosh_recommended true
    end
  end
end
