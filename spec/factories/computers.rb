# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :computer do
    name { Faker::Lorem.words(3) }
    description { Faker::Lorem.sentences(6) }

    trait :published do

      after(:create) do |c|
        c.hull = create(:hull, :fitting)
        c.motherboard  = create(:motherboard, :fitting)
        c.power_supply = create(:power_supply, :fitting)
        c.cpus = PackSet.create_with_hardware(create(:cpu, :fitting))
        c.ssds = PackSet.create_with_hardware(create(:ssd))
        c.hdds = PackSet.create_with_hardware(create(:hdd))
        c.video_cards = PackSet.create_with_hardware(create(:video_card, :fitting))
        c.rams = PackSet.create_with_hardware(create(:ram, :fitting))
        c.cpu_coolers = PackSet.create_with_hardware(create(:cpu_cooler, :fitting))
        c.optical_drives = PackSet.create_with_hardware(create(:optical_drive))
        c.publish
      end
    end
  end
end
