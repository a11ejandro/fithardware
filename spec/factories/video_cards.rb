# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :video_card, class: VideoCard, parent: :hardware do
    chipset { Faker::Lorem.word }
    chipset_manufacturer VideoCard::CHIPSET_MANUFACTURERS.sample
    chipset_frequency { rand(600..1500) }
    memory_size { rand(1024..4096) }
    memory_frequency { rand(1500..7000) }
    memory_type VideoCard::MEMORY_TYPES.sample
    number_of_6_pin 2
    number_of_8_pin 2
    performance_score { rand(1000..10000) }
    multi_gpu_technology VideoCard::MULTI_GPU_TECHNOLOGIES.sample
    number_of_supported_monitors { rand(1..4)}
    max_resolution { "#{rand(2000...4500)}x#{rand(1000..2500)}"}
    number_of_universal_processors {rand(1000..5000)}

    trait :fitting do
      multi_gpu_technology 'CrossFire'
      chipset_manufacturer 'NVIDIA'
      unpacked_length 250
      unpacked_width 100
      power_consumption 100
    end
  end
end
