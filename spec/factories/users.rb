# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    username { Faker::Name.name}
    email      { Faker::Internet.email }
    password  'password'
  end

  trait :admin do
    admin true
    email 'admin@email.com'
  end
end
