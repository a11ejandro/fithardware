# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :motherboard, class: Motherboard, parent: :hardware do
    form_factor Motherboard::POSSIBLE_FORM_FACTORS.sample
    socket { "Socket " + rand(3).to_s }
    chipset { Faker::Lorem.word }
    ram_generations { [Ram::GENERATIONS.sample] }
    supported_multi_gpus  { [VideoCard::MULTI_GPU_TECHNOLOGIES.sample] }
    number_of_ram_slots { rand(1..4) }
    number_of_pcie_x16_slots { rand(1..4) }
    integrated_audio { Faker::Lorem.word }
    number_of_sata2_ports { rand(2..6) }
    power_consumption { rand(20..40) }
    integrated_gpu { Faker::Lorem.word }
    max_ram_frequency {[1333..2444].sample }

    trait :fitting do
      number_of_sata2_ports 3
      number_of_sata3_ports 2
      number_of_pcie_x16_slots 2
      number_of_pcie_x1_slots 2
      number_of_pcie_x8_slots 2
      form_factor "microATX"
      socket "Fitting Socket"
      ram_generations ["DDR3"]
      max_ram_total_size 16384
      max_ram_frequency 1333
      supported_multi_gpus ['CrossFire']
    end
  end
end
