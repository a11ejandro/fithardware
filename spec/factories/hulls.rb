# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hull, class: Hull, parent: :hardware do
    form_factors { [ Motherboard::POSSIBLE_FORM_FACTORS.sample, Motherboard::POSSIBLE_FORM_FACTORS.sample ] }
    hackintosh_recommended true
    number_of_storage_slots  { rand(2..6)}
    number_of_optical_drives { rand(2..6)}


    trait :fitting do
      number_of_storage_slots 4
      number_of_optical_drives 2
      form_factors ["microATX", "ATX"]
      video_card_max_length 270
      cpu_cooler_max_height 150
      unpacked_width 200
    end
  end
end
