# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :power_supply, class: PowerSupply, parent: :hardware do
    power { rand(250..1000) }
    number_of_6_pin { rand(5) }
    number_of_8_pin { rand(5) }
    number_of_sata_connectors { rand(2..6) }

    trait :fitting do
      power 1000
      number_of_6_pin 4
      number_of_8_pin 4
      number_of_sata_connectors 6
    end
  end
end
