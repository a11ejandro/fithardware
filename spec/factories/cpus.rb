# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cpu, class: Cpu, parent: :hardware do
    manufacturer { Cpu::MANUFACTURERS.sample }
    complectation { Cpu::COMPLECTATIONS.sample }
    socket { "Socket " + rand(3).to_s }
    power_consumption { rand(60..120) }
    frequency { rand(1100..3300) }
    turbo_frequency 3400
    architecture { Faker::Lorem.word }
    manufacturing_process  { rand(22..45) }
    l2_cache_size { rand(1..4) }
    l3_cache_size { rand(2..16) }
    integrated_gpu { Faker::Lorem.word }
    number_of_cores { rand(1..8) }

    trait :fitting do
      socket "Fitting Socket"
      complectation 'Box'
    end
  end
end
