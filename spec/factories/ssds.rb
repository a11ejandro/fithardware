# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ssd, class: Ssd, parent: :hardware do
    capacity { rand(256..2048) }
    sata3_support true
    power_consumption 4
    read_speed { rand(300..600) }
    write_speed {rand(300..600) }
    random_write_speed { rand(30000..60000) }
  end
end
