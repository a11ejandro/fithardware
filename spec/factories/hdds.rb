# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hdd, class: Hdd, parent: :hardware do
    capacity { rand(256..4096) }
    number_of_platters { rand(1..4) }
    rpm [5400, 7200].sample
    cache_size {rand(16..256)}
    sata3_support [true, false].sample
    power_consumption 7
  end
end
