require 'spec_helper'

describe Motherboard do
  [:chipset, :socket, :number_of_sockets, :number_of_ram_slots, :number_of_pcie_x16_slots].each do |field|
    it { should validate_presence_of field }
  end
  it { should validate_inclusion_of(:form_factor).to_allow(described_class::POSSIBLE_FORM_FACTORS) }

  describe 'RAM generations validator' do
    before do
      @valid_2_generations_board = build :motherboard, ram_generations: ['DDR2' ,'DDR3']
      @valid_1_generation_board = build :motherboard, ram_generations: ['DDR3']
      @invalid_2_generations_board = build :motherboard, ram_generations: ['DDR3', 'DDR100500']
      @invalid_1_generation_board = build :motherboard, ram_generations: ['DDR100500']
    end

    specify do
      @valid_1_generation_board.should be_valid
      @valid_2_generations_board.should be_valid

      @invalid_1_generation_board.should_not be_valid
      @invalid_2_generations_board.should_not be_valid
    end
  end


  describe 'scopes' do

    before do
      5.times { create :motherboard, :fitting }
    end

    context 'with form factor' do
      before { @dummy_board = create :motherboard, form_factor: 'microATX' }
      specify { Motherboard.with_form_factor('ATX').to_a.should_not include @dummy_board }
    end

    context 'with socket' do
      before { @socket_board = create :motherboard, socket: 'socket' }
      specify { Motherboard.with_socket('socket').to_a.should == [@socket_board] }
    end

    context 'with multi gpu technologies' do
      before do
        @sli_board = create :motherboard, supported_multi_gpus: ['SLI', 'CrossFire']
        @cross_fire_board = create :motherboard, supported_multi_gpus: ['CrossFire']
      end
      specify { Motherboard.with_supported_multi_gpus(['SLI']).to_a.should == [@sli_board] }
    end

    context 'with number of pcie x16 slots' do
      before do
        @six_video_board = create :motherboard, number_of_pcie_x16_slots: 6
        @five_video_board = create :motherboard, number_of_pcie_x16_slots: 5
      end
      specify { Motherboard.with_number_of_pcie_x16_slots(5).to_a.count.should == 1 }
    end

    context 'with number of pcie x8 slots' do
      before do
        @six_slot_board = create :motherboard, number_of_pcie_x8_slots: 6
        @five_video_board = create :motherboard, number_of_pcie_x8_slots: 5
      end
      specify { Motherboard.with_number_of_pcie_x8_slots(5).to_a.count.should == 1 }
    end

    context 'with number of pcie x1 slots' do
      before do
        @six_slot_board = create :motherboard, number_of_pcie_x1_slots: 6
        @five_video_board = create :motherboard, number_of_pcie_x1_slots: 5
      end
      specify { Motherboard.with_number_of_pcie_x1_slots(5).to_a.count.should == 1 }
    end


    context 'with number of RAM slots' do
      before do
        @five_ram_board = create :motherboard, number_of_ram_slots: 5
        @six_ram_board = create :motherboard, number_of_ram_slots: 6
      end
      specify { Motherboard.with_number_of_ram_slots(5).to_a.count.should == 1 }
    end
  end

  describe 'methods' do

    context 'computer_compatible' do
      before do
        @computer = create :computer, :published
        @uncompatible_motherboard = create :motherboard, ram_generations: ['DDR2']
        @compatible_motherboard = create :motherboard, :fitting
      end
      specify { Motherboard.computer_compatible(@computer).to_a.should_not include @uncompatible_motherboard }
      specify { Motherboard.computer_compatible(@computer).to_a.should include @compatible_motherboard }
    end

    describe 'filter_values' do
      before do
        @motherboard1 = create :motherboard, manufacturer: 'Asus', chipset: 'B75'
        @motherboard2 = create :motherboard, manufacturer: 'Asus', chipset: 'B75'
        @motherboard3 = create :motherboard, manufacturer: 'AsRock', chipset: 'B75'
      end
      specify 'should return all existent values for fields' do
        Motherboard.filter_values[:manufacturer].should == ['AsRock', 'Asus']
      end
      specify 'should not return fields with only one value' do
        Motherboard.filter_values[:chipset].should == nil
      end
    end
  end
end
