require 'spec_helper'

describe Hdd do
  [:capacity, :rpm, :cache_size, :sata3_support].each do |field|
    it { should validate_presence_of field}
  end

  describe 'Methods' do
    context 'computer_compatible' do
      before do
        @computer = create :computer, :published
        @hdd1 = create :hdd
        @hdd2 = create :hdd
      end
      specify 'no restrictions for compatibility with computer' do
        Hdd.computer_compatible(@computer).to_a.should include @hdd1, @hdd2
      end
    end

    context 'filter_values' do
      before do
        @hdd1 = create :hdd, cache_size: 32, rpm: 7200
        @hdd2 = create :hdd, cache_size: 32, rpm: 5400
      end

      specify 'should not return fields with only one value' do
        Hdd.filter_values[:cache_size].should == nil
      end

      specify 'should return all existent values for fields' do
        Hdd.filter_values[:rpm].should == [5400, 7200]
      end
    end
  end
end
