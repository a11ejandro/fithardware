require 'spec_helper'

describe Computer do
  it { should be_timestamped_document }
  it { should validate_presence_of :rams  }
  it { should validate_presence_of :motherboard  }
  it { should validate_presence_of :hull }
  it { should validate_presence_of :cpus }
  it { should validate_presence_of :power_supply }
  it { should validate_presence_of :power_consumption }
  it { should belong_to :hull }
  it { should belong_to :motherboard }
  it { should belong_to :power_supply }
  it { should have_one :cpus }
  it { should have_one :rams }
  it { should have_one :video_cards }
  it { should have_one :ssds }
  it { should have_one :ssds }

  before { @computer = create :computer, :published }
  specify "Computer by default is built from compatible parts" do
    @computer.should be_valid
  end

  describe 'scopes' do
    context 'published'
    before do
      5.times { create :computer }
    end

    specify { Computer.published.to_a.should==[@computer] }
  end

  describe "Hardware mismatch" do

    context "Hull and Motherboard form-factors" do
      before { @computer.hull.form_factors = ['EATX'] }
      it_should_behave_like 'unfittable computer'
    end


    context "Motherboard and RAM generations" do
      before { @computer.motherboard.ram_generations = ['DDR2'] }
      it_should_behave_like 'unfittable computer'
    end

    context "Motherboard and total ram capacity" do
      before { @computer.motherboard.max_ram_total_size = 1 }
      it_should_behave_like 'unfittable computer'
    end

    context "Motherboard and ram frequencies" do
      before { @computer.rams.first.frequency = 2444}
      it_should_behave_like 'unfittable computer'
    end

    context "RAM frequencies" do
      before { @computer.rams << build(:ram, frequency: 999) }
      it_should_behave_like 'unfittable computer'
    end
  end

  describe "Unfitting sizes" do

    describe "cpu cooler and case width"
      context "third-party cooler" do
        before do
          @computer.cpu_coolers.first.unpacked_height = 300
          @computer.cpus.first.complectation = 'Tray'
        end
        it_should_behave_like 'unfittable computer'
      end

      context "cpu cooler out of box" do
        before do
          @computer.cpus.first.unpacked_height = 300
          @computer.cpu_coolers.packs = nil
        end
        it_should_behave_like 'unfittable computer'
      end

    describe "videocard" do
      context "width" do
        before { @computer.video_cards.first.unpacked_width = 300 }
        it_should_behave_like 'unfittable computer'
      end

      context "length" do
        before { @computer.video_cards.first.unpacked_length = 700 }
        it_should_behave_like 'unfittable computer'
      end
    end
  end

  describe "Not enough slots" do
    context "for RAM" do
      before { @computer.motherboard.number_of_ram_slots = 1 }
      before { @computer.rams << build(:ram, :fitting) }
      it_should_behave_like 'unfittable computer'
    end

    context "for Videocards" do
      before { 4.times {@computer.video_cards << build(:video_card)} }
      it_should_behave_like 'unfittable computer'
    end

    context "SATA ports" do
      before { @computer.motherboard.number_of_sata2_ports = @computer.motherboard.number_of_sata3_ports = 0 }
      it_should_behave_like 'unfittable computer'
    end
  end

  describe "Power supply" do
    context "not enough necessary connectors" do
      # This test should include the check for number of necessary connectors for each part
      before { @computer.power_supply.number_of_6_pin = @computer.power_supply.number_of_8_pin = 0 }
      it_should_behave_like 'unfittable computer'
    end

    context "not enough power" do
      before do
        @computer.power_supply.power = 20
      end
      it_should_behave_like 'unfittable computer'
    end
  end

  describe "Without storage" do
    before { @computer.ssds.packs = @computer.hdds.packs = nil }
    it_should_behave_like 'unfittable computer'
  end

  describe "SLI/Crossfire" do
    before do
      @computer.motherboard.number_of_pcie_x16_slots = 2
      @computer.motherboard.supported_multi_gpus = ["SLI"]
    end

    context "motherboard and videocards with different technologies" do
      before do
        @computer.video_cards.first.multi_gpu_technology = 'CrossFire'
        @computer.video_cards << build(:video_card, multi_gpu_technology: 'CrossFire')
      end
      it_should_behave_like 'unfittable computer'
    end

    context "system has only one videocard" do
      before { @computer.video_cards.first.multi_gpu_technology = 'CrossFire' }
      specify { @computer.publish.should eq true }
    end

    context "videocards with different technologies" do
      before do
        @computer.motherboard.supported_multi_gpus = ['SLI', 'CrossFire']
        @computer.video_cards.first.multi_gpu_technology = 'SLI'
        @computer.video_cards << build(:video_card, multi_gpu_technology: 'CrossFire')
      end
      it_should_behave_like 'unfittable computer'
    end
  end
end
