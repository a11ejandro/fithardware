require 'spec_helper'

describe CpuCooler do
  it { should be_timestamped_document }
  it { should validate_presence_of :sockets }
  it { should validate_inclusion_of(:form_factor).to_allow(described_class::FORM_FACTORS) }

  describe 'scopes' do
    before do
      5.times {create :cpu_cooler, :fitting}
    end

    context 'with form factor' do
      before { create :cpu_cooler, form_factor: 'Liquid'}
      specify { CpuCooler.with_form_factor('Liquid').to_a.count.should eq 1 }
    end

    context 'with socket' do
      before { create :cpu_cooler, sockets: ['Blah', 'Blah-Blah'] }
      specify { CpuCooler.with_sockets(['Blah']).to_a.count.should eq 1 }
    end
  end

  describe 'methods' do
    context 'computer compatible' do
      before do
        @computer = create :computer, :published
        @uncompatible_cooler = create :cpu_cooler
        @unfitting_cooler = create :cpu_cooler, :fitting, unpacked_height: 400
        @compatible_cooler = create :cpu_cooler, :fitting
        @collection = CpuCooler.computer_compatible(@computer).to_a
      end

      specify do
        @collection.should_not include(@uncompatible_cooler, @unfitting_cooler)
        @collection.should include(@compatible_cooler)
      end
    end

    context 'filter values' do
      before do
        3.times {create :cpu_cooler, :fitting }
        @unique_cooler = create :cpu_cooler, form_factor: 'Liquid', sockets: ['Fitting Socket'], unpacked_height: 240
        @collection = CpuCooler.filter_values
      end

      specify do
        @collection[:sockets].should eq nil
        @collection[:unpacked_height].should eq [120, 240]
      end
    end
  end

end