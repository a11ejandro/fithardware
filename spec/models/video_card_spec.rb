require 'spec_helper'

describe VideoCard do
  [:memory_size, :performance_score, :chipset, :chipset_manufacturer, :chipset_frequency, :memory_type,
      :memory_frequency, :system_bus_bit, :number_of_universal_processors].each do |field|
    it { should validate_presence_of field }
  end

  it { should validate_inclusion_of(:chipset_manufacturer).to_allow(described_class::CHIPSET_MANUFACTURERS) }
  it { should validate_inclusion_of(:multi_gpu_technology).to_allow(described_class::MULTI_GPU_TECHNOLOGIES) }
  it { should validate_inclusion_of(:memory_type).to_allow(described_class::MEMORY_TYPES) }

  describe 'scopes' do

  end

  describe 'methods' do
    describe 'computer compatible' do
      before do
        @computer = create :computer, :published
        @uncompatible_hardware = create :video_card, multi_gpu_technology: 'SLI'
        @compatible_hardware = create :video_card, :fitting
      end

      specify do
        VideoCard.computer_compatible(@computer).to_a.should include @compatible_hardware
        VideoCard.computer_compatible(@computer).to_a.should_not include @uncompatible_hardware
      end
    end

    describe 'filter values' do
      before do
        @vc1 = create :video_card, :fitting
        @vc2 = create :video_card, :fitting, number_of_6_pin: 4
      end

      specify 'should return all existent values for fields' do
        VideoCard.filter_values[:number_of_6_pin].should == [2, 4]
      end
      specify 'should not return fields with only one value' do
        VideoCard.filter_values[:multi_gpu_technology].should == nil
      end
    end
  end
end
