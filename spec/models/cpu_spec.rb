require 'spec_helper'

describe Cpu do
  it { should be_timestamped_document }

  [:frequency, :manufacturing_process, :architecture, :socket, :number_of_cores,
   :l2_cache_size, :l3_cache_size].each do |field|
    it { should validate_presence_of field }
  end

  it { should validate_inclusion_of(:manufacturer).to_allow(described_class::MANUFACTURERS) }
  it { should validate_inclusion_of(:complectation).to_allow(described_class::COMPLECTATIONS) }

  describe 'scopes' do
    before do
      @amd = create :cpu, manufacturer: 'AMD'
      @intel = create :cpu, manufacturer: 'Intel'
      @fitting = create :cpu, :fitting, manufacturer: 'AMD'
    end

    context 'amd' do
      specify { Cpu.amd.to_a.count.should == 2 }
    end

    context 'intel' do
      specify { Cpu.intel.to_a.count.should == 1 }
    end

    context 'with socket' do
      specify { Cpu.with_socket('Fitting Socket').to_a.count.should == 1 }
    end
  end

  describe 'methods' do
    context 'computer_compatible' do
      before do
        @computer = create :computer, motherboard: create(:motherboard, :fitting)
        @uncompatible_cpu = create :cpu, socket: 'LGA1'
        @compatible_cpu = create :cpu, :fitting
      end
      specify { Cpu.computer_compatible(@computer).to_a.should_not include @uncompatible_cpu }
      specify { Cpu.computer_compatible(@computer).to_a.should include @compatible_cpu}
    end

    describe 'filter_values' do
      before do
        @cpu1 = create :cpu, manufacturer: 'Intel', socket: 'LGA 2011'
        @cpu2 = create :cpu, manufacturer: 'Intel', socket: 'LGA 1355'
      end
      specify 'should return all existent values for fields' do
        Cpu.filter_values[:manufacturer].should == nil
      end
      specify 'should not return fields with only one value' do
        Cpu.filter_values[:socket].should == ['LGA 1355', 'LGA 2011']
      end
    end
  end
end
