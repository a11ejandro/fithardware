require 'spec_helper'

describe Ram do
  [:capacity, :frequency, :generation].each do |field|
    it { should validate_presence_of field }
  end

  it { should validate_inclusion_of(:generation).to_allow(described_class::GENERATIONS) }

  describe 'scopes' do
    before { @dumb_ram = create :ram, generation: 'DDR2', capacity: 1024 }

    context 'with capacity' do
      before { @necessary_ram = create :ram, capacity: 4096 }
      specify { Ram.with_capacity(4096).to_a.should == [@necessary_ram] }
    end

    context 'with generation' do
      before { @necessary_ram = create :ram, generation: 'DDR3' }
      specify { Ram.with_generation('DDR3').to_a.should == [@necessary_ram] }
    end
  end

  describe 'methods' do

    context 'computer_compatible' do
      before do
        @computer = create :computer, motherboard: create(:motherboard, :fitting)
        @uncompatible_ram = create :ram, generation: 'DDR2'
        @compatible_ram = create :ram, :fitting
      end
      specify { Ram.computer_compatible(@computer).to_a.should_not include @uncompatible_ram }
      specify { Ram.computer_compatible(@computer).to_a.should include @compatible_ram}
    end

    describe 'filter_values' do
      before do
        @ram1 = create :ram, manufacturer: 'Asus', generation: 'DDR3'
        @ram2 = create :ram, manufacturer: 'Asus', generation: 'DDR3'
        @ram3 = create :ram, manufacturer: 'Samsung', generation: 'DDR3'
      end
      specify 'should return all existent values for fields' do
        Ram.filter_values[:manufacturer].should == ['Asus', 'Samsung']
      end
      specify 'should not return fields with only one value' do
        Ram.filter_values[:generation].should == nil
      end
    end
  end
end
