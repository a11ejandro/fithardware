require 'spec_helper'

describe OpticalDrive do
  it { should validate_presence_of :cd_read }

  describe 'Methods' do
    context 'computer_compatible' do
      before do
        @computer = create :computer, :published
        @optical_drive1 = create :optical_drive
        @optical_drive2 = create :optical_drive
      end
      specify 'no restrictions for compatibility with computer' do
        OpticalDrive.computer_compatible(@computer).to_a.should include @optical_drive1, @optical_drive2
      end
    end

    context 'filter_values' do
      before do
        @optical_drive1 = create :optical_drive, cd_read: 48, cd_r_write: 12
        @optical_drive2 = create :optical_drive, cd_read: 48, cd_r_write: 24
      end

      specify 'should not return fields with only one value' do
        OpticalDrive.filter_values[:cd_read].should == nil
      end

      specify 'should return all existent values for fields' do
        OpticalDrive.filter_values[:cd_r_write].should == { min: 12, max: 24 }
      end
    end
  end
end

