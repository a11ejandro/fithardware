require 'spec_helper'

describe Hardware do
  it { should be_timestamped_document }
  it { should validate_presence_of :product_name }
  it { should validate_presence_of :manufacturer }
  it { should validate_numericality_of :power_consumption }
  it { should validate_uniqueness_of :amazon_asin }

  describe 'scopes' do
    context 'hackintosh recommended' do
      before do
        @recommended = create :hardware, :hackintosh_recommended
        @not_recommended = create :hardware, hackintosh_recommended: false
      end
      specify { Hardware.hackintosh_compatible.to_a.should == [@recommended] }
    end

    context 'by_performance' do
      before do
        @higher_performance = create :hardware, performance_score: 9000
        @lower_performance = create :hardware, performance_score: 3
      end
      specify  { Hardware.by_performance.to_a.should == [@higher_performance, @lower_performance] }
    end

    context 'quiet' do
      before do
        @noise_weapon = create :hardware, noise_level: 200
        @quiet = create :hardware, noise_level: 20
      end
      specify { Hardware.quiet.to_a.should == [@quiet] }
    end

  end

  describe 'methods' do
    describe 'update_from_amazon' do
      context  'data retrieving success' do
        before do
          @dummy_hardware = build :hardware, amazon_asin: 'B009O7YZAG'
          @dummy_hardware.parse_from_services
        end

        specify do
          @dummy_hardware.should be_valid
          @dummy_hardware.manufacturer.should == 'Gigabyte'
          @dummy_hardware.product_name.should == 'GA-F2A55M-HD2'
          @dummy_hardware.amazon_link.should_not be_nil
          @dummy_hardware.amazon_price.should_not be_nil
        end
      end
    end
  end

end
