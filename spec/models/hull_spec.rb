require 'spec_helper'

describe Hull do
  it { should validate_presence_of :form_factors }

  describe 'only real form-factors are valid' do
    before { @hull = build :hull, form_factors: ['ATX', 'HyperMegaATX-3000'] }
    specify { @hull.should_not be_valid }
  end

  describe 'create embedded power supply' do
    before {@hull = create :hull, power_supply_power: 300, manufacturer: 'CoolerMaster'}

    specify do
      @hull.power_supply.power.should eq 300
      @hull.power_supply.manufacturer.should eq 'CoolerMaster'
    end
  end

  describe 'scopes' do
    context 'with form factor' do
      before do
        @hull_1 = create :hull, form_factors: ['ATX', 'EATX']
        @hull_2 = create :hull, form_factors: ['EATX', 'microATX']
      end
      specify { Hull.with_form_factors(['ATX']).to_a.should == [@hull_1] }
    end
  end

  describe 'methods' do
    before do
      @computer = create :computer, :published
      @computer.hull.delete
    end
    describe 'computer compatible' do
      context 'by dimensions' do
        before do
          @compatible_hull = create :hull, :fitting
          @uncompatible_by_width_hull = create :hull, :fitting, cpu_cooler_max_height: 20
          @uncompatible_by_length_hull = create :hull, :fitting, video_card_max_length: 100
        end


        specify { Hull.computer_compatible(@computer).to_a.should eq [@compatible_hull]}
      end

      context 'by number of slots' do
        before do
          @compatible_hull = create :hull, :fitting
          @uncompatible_by_storage_hull = create :hull, :fitting, number_of_storage_slots: 1
          @uncompatible_by_optical_drives_hull = create :hull, :fitting, number_of_optical_drives: 0
        end

        specify { Hull.computer_compatible(@computer).to_a.should eq [@compatible_hull]}
      end

      context 'by power_supply' do
        before do
          @computer.power_supply = nil
          @computer.video_cards = PackSet.create_with_hardware build(:video_card, :fitting, number_of_6_pin: 1, number_of_8_pin: 1)
          @compatible_hull = create :hull, :fitting, power_supply_power: 500
          @potentially_compatible_hull = create :hull, :fitting
          @uncompatible_hull = create :hull, :fitting, power_supply_power: 50
        end

        specify do
          Hull.computer_compatible(@computer).to_a.should include(@compatible_hull, @potentially_compatible_hull)
          Hull.computer_compatible(@computer).to_a.should_not include @uncompatible_hull
        end
      end
    end
    
    describe 'filter values' do
      before do
        @hull_1 = create :hull, :fitting
        @hull_2 = create :hull, :fitting, number_of_optical_drives: 3
      end

      specify 'should not return fields with only one value' do
        Hull.filter_values[:number_of_storage_slots].should == nil
      end

      specify 'should return all existent values for fields' do
        Hull.filter_values[:number_of_optical_drives].should == [2, 3]
      end
    end
  end
end
