require 'spec_helper'

describe PowerSupply do
  it { should validate_presence_of :power }
  it { should validate_presence_of :number_of_sata_connectors }
  it { should validate_inclusion_of(:certificate_80_plus).to_allow(described_class::CERTIFICATES)}

  describe 'methods' do
    describe 'computer_compatible' do
      before { @computer = create :computer, :published }
      context 'by video card connectors' do
        before do
          @computer.video_cards.first.number_of_6_pin = 5
          @computer.video_cards.first.number_of_8_pin = 5
          @power_supply1 = create :power_supply, :fitting, power: 500, number_of_6_2_pin: 12
          @power_supply2 = create :power_supply, :fitting, power: 1000, number_of_6_2_pin: 0
          @power_supply3 = create :power_supply, :fitting, power: 1000, number_of_6_2_pin: 10
        end
        specify do
          PowerSupply.computer_compatible(@computer).to_a.should_not include @power_supply2
          PowerSupply.computer_compatible(@computer).to_a.should include(@power_supply1, @power_supply3)
        end
      end

      context 'by sata connectors' do
        before do
          @computer.hdds.packs.first.count = 8
          @power_supply1 = create :power_supply, :fitting
          @power_supply2 = create :power_supply, :fitting, number_of_sata_connectors: 10
        end

        specify {  PowerSupply.computer_compatible(@computer).to_a.should eq [@power_supply2]}
      end
    end

    context 'filter_values' do
      before do
        @power_supply1 = create :power_supply, power: 500, number_of_6_pin: 2
        @power_supply2 = create :power_supply, power: 500, number_of_6_pin: 3
      end

      specify 'should not return fields with only one value' do
        PowerSupply.filter_values[:power].should == nil
      end

      specify 'should return all existent values for fields' do
        PowerSupply.filter_values[:number_of_6_pin].should == [2, 3]
      end
    end
  end
end
