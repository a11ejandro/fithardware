require 'spec_helper'

describe Ssd do
  [:capacity, :sata3_support, :random_write_speed].each do |field|
    it { should validate_presence_of field }
  end

  describe 'Methods' do
    context 'computer_compatible' do
      before do
        @computer = create :computer, :published
        @ssd1 = create :ssd
        @ssd2 = create :ssd
      end
      specify 'no restrictions for compatibility with computer' do
        Ssd.computer_compatible(@computer).to_a.should include @ssd1, @ssd2
      end
    end

    context 'filter_values' do
      before do
        @ssd1 = create :ssd, capacity: 64, random_write_speed: 550000
        @ssd2 = create :ssd, capacity: 64, random_write_speed: 540000
      end

      specify 'should not return fields with only one value' do
        Ssd.filter_values[:capacity].should == nil
      end

      specify 'should return all existent values for fields' do
        Ssd.filter_values[:random_write_speed].should == { min: 540000, max: 550000 }
      end
    end
  end
end

