require 'spec_helper'

describe 'Authentication' do
  describe 'admin' do
    context 'sign in' do
      When { login_as_admin }
      Then do
        expect(page).to have_content 'Signed in successfully.'
        expect(page).to have_content 'Computers'
        expect(page).to have_content 'Hardware'
        expect(page).to have_content 'Users'
        expect(page).to have_content 'My Computers'
        expect(page).to have_content 'My Profile'
        expect(page).to have_content 'Sign Out'

        page.should_not have_content 'Sign In'
        page.should_not have_content 'Sign Up'
      end
    end
  end

  describe 'user' do
    context 'sign in' do
      When { login_as_user }
      Then do
        expect(page).to have_content 'Signed in successfully.'
        expect(page).to have_content 'My Computers'
        expect(page).to have_content 'My Profile'
        expect(page).to have_content 'Sign Out'

        page.should_not have_content 'Sign In'
        page.should_not have_content 'Sign Up'
      end
    end

    context 'sign up' do
      Given { visit new_user_registration_path }

      When do
        fill_in 'user_email', with: 'correct@email.com'
        fill_in 'user_username', with: 'I. Pupkin'
        fill_in 'user_password', with: 'qwerty123'
        fill_in 'user_password_confirmation', with: 'qwerty123'

        click_on 'Sign up!'
      end

      Then { User.last.email.should == 'correct@email.com' }
      And { User.last.admin.should be false }
    end

    context 'own computers' do
      When do
        @user = create :user
        login_as_user(@user)
        @user_comp = create :computer, user: @user
        @alien_comp = create :computer, :published
        visit user_computers_path(@user)
      end

      Then do
        page.should have_content @user_comp.description
        page.should_not have_content @alien_comp.description
      end
    end
  end
end

describe 'Authorization' do
  describe 'admin user' do
    describe 'managing users' do
      before { login_as_admin }

      context 'admin should be unable to delete himself from user index' do
        When { visit users_path }
        Then { page.should_not have_selector('btn btn-danger', text: 'Destroy')}
      end

      context 'admin can destroy users', js: true do
        When do
          create :user, username: 'Ioann Vasiljevich'
          visit users_path
          click_on 'Destroy'
          page.driver.browser.switch_to.alert.accept
        end

        Then { page.should_not have_content 'Ioann Vasiljevich' }
      end
    end
  end

  describe 'user' do

    before do
      @user = create :user
      @computer = create_published_computer(@user)
    end

    describe 'editing published computer' do
      before do
        login_as_user(@user)
        visit edit_computer_path(@computer)
      end

      context 'removing hardware that setups single' do
        When do
          within '#motherboards' do
            click_on 'Remove'
          end
        end
        Then do
          page.should_not have_content 'Verified'
          within '#motherboards' do
            page.should_not have_selector('.hardware-container')
          end
        end
      end

      context 'removing packed hardware' do
        When do
          within '#cpus' do
            click_on 'Remove'
          end
        end
        Then do
          page.should_not have_content 'Verified'
          within '#cpus' do
            page.should_not have_selector('.hardware-container')
          end
        end
      end
    end
  end
end