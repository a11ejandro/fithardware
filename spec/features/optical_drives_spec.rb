require 'spec_helper'

describe 'Optical Drives' do

  describe 'index' do
    context 'without Optical Drives' do
      When { visit optical_drives_path }
      it_should_behave_like 'absent items'
    end

    describe 'with items present' do
      When { visit optical_drives_path}
      it_should_behave_like 'orderable list', :optical_drive
    end

    describe 'guide', js: true do
      Given { visit optical_drives_path }
      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_optical_drive = create :optical_drive, cd_read: 36, dvd_read: 40
      @also_visible_optical_drive = create :optical_drive, cd_read: 36, dvd_read: 30
      @unnecessary_optical_drive = create :optical_drive, cd_read: 36, dvd_read: 20
    end

    context 'should offer options that have more than one possible value ' do
      When { visit optical_drives_path }
      Then do
        within '#filters' do
          page.should have_content 'DVD read speed, x1.385 Mb/s'
          page.should_not have_content 'CD read speed, x150 Kb/s'
        end
      end
    end

    context 'should display filtered optical_drives' do
      When { visit optical_drives_path }
      When  do
        within '#filters' do
          fill_in '_with_dvd_read_min', with: 25
          fill_in '_with_dvd_read_max', with: 40
          click_button 'Filter'
        end
      end
      Then do
        find_field('_with_dvd_read_min').value.should eq '25'
        page.all(".hardware-container").count.should eq 2
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :optical_drive }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new optical_drive' do
    Given { login_as_admin }
    Given { visit new_optical_drive_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('optical_drive')

        fill_in 'optical_drive_cd_read', with: 56
        fill_in 'optical_drive_cd_r_write', with: 900

        click_on 'Create Optical drive'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { OpticalDrive.last.cd_r_write.should == 900 }
    end

    context 'parsing from Yandex Market' do
      When { initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=6301440&hid=91035', 'optical_drive')}

      Then do
        find_field('optical_drive[cd_read]').value.should eq '48'
        find_field('optical_drive[cd_r_write]').value.should eq '48'
        find_field('optical_drive[cd_rw_write]').value.should eq '48'
        find_field('optical_drive[dvd_read]').value.should eq '16'
        find_field('optical_drive[dvd_r_write]').value.should eq '16'
        find_field('optical_drive[dvd_rw_write]').value.should eq '6'
        find_field('optical_drive[dvd_dl_write]').value.should eq '8'
        find_field('optical_drive[bd_read]').value.should eq '8'
        find_field('optical_drive[bd_r_write]').value.should eq '12'
        find_field('optical_drive[bd_re_write]').value.should eq '2'
        find_field('optical_drive[bd_dl_write]').value.should eq '2'
      end
    end

    it_should_behave_like 'amazon fetchable', 'optical_drive'
  end
end
