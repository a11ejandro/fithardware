require 'spec_helper'

describe 'SSDs' do

  describe 'index' do
    context 'without SSDs' do
      When { visit ssds_path }
      it_should_behave_like 'absent items'
    end

    describe 'with items present' do
      When { visit ssds_path}
      it_should_behave_like 'orderable list', :ssd
    end

    describe 'guide', js: true do
      Given { visit ssds_path }
      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_ssd = create :ssd, capacity: 120, write_speed: 400
      @also_visible_ssd = create :ssd, capacity: 240, write_speed: 400
      @unnecessary_ssd = create :ssd, capacity: 60, write_speed: 400
    end

    context 'should offer options that have more than one possible value ' do
      When { visit ssds_path }
      Then do
        within '#filters' do
          page.should have_content 'Capacity, Gb'
          page.should_not have_content 'Write speed, Mb/s'
        end
      end
    end

    context 'should display filtered ssds' do
      When { visit ssds_path }
      When  do
        within '#filters' do
          fill_in '_with_capacity_min', with: 100
          fill_in '_with_capacity_max', with: 300
          click_button 'Filter'
        end
      end
      Then do
        find_field('_with_capacity_min').value.should eq '100'
        page.all(".hardware-container").count.should eq 2
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :ssd }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new ssd' do
    Given { login_as_admin }
    Given { visit new_ssd_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('ssd')

        fill_in 'ssd_capacity', with: 500
        fill_in 'ssd_write_speed', with: 450
        fill_in 'ssd_read_speed', with: 450
        fill_in 'ssd_random_write_speed', with: 9000
        check 'ssd_sata3_support'

        click_on 'Create Ssd'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { Ssd.last.random_write_speed.should == 9000 }
    end

    context 'parsing from Yandex Market' do
      When {initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=8526053&hid=91033', 'ssd')}

      Then do
        find_field('ssd[capacity]').value.should eq '120'
        find_field('ssd[read_speed]').value.should eq '450'
        find_field('ssd[write_speed]').value.should eq '450'
        find_field('ssd[random_write_speed]').value.should eq '55000'
        find_field('ssd[sata3_support]').should be_checked
        find_field('ssd[power_consumption]').value.should eq '2.05'
      end
    end

    it_should_behave_like 'amazon fetchable', 'ssd'
  end
end
