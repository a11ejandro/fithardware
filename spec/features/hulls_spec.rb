require 'spec_helper'

describe 'Hulls' do

  describe 'index' do
    context 'without Video cards' do
      When { visit hulls_path }
      it_should_behave_like 'absent items'
    end

    describe 'with items present' do
      When { visit hulls_path}
      it_should_behave_like 'orderable list', :hull
    end

    describe 'guide', js: true do
      Given { visit hulls_path }
      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_hull = create :hull, :fitting
      @also_visible_hull = create :hull, :fitting
      @unnecessary_hull = create :hull, :fitting, number_of_storage_slots: 1
    end

    context 'should offer options that have more than one possible value ' do
      When { visit hulls_path }
      Then do
        within '#filters' do
          page.should have_content 'Number of Storage slots'
          page.should_not have_content 'Number of Optical Drive bays'
        end
      end
    end

    context 'should display filtered power supplies' do
      When { visit hulls_path }
      When  do
        within '#filters' do
          find('span', text: 'Number of Storage slots').click
          within('._with_number_of_storage_slots') { check '4'}
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_number_of_storage_slots_4').should be_checked
        page.all(".hardware-container").count.should eql(2)
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :hull, :fitting, power_supply_power: 600 }

    Then do
      visit edit_computer_path(@computer)
      within '#hulls' do
        find('.btn').click
      end
      click_on 'Add'
      page.all('.hardware-container').count.should eq 3
      within '#hulls' do
        click_on 'Remove'
      end
      page.should_not have_content @hardware.manufacturer
    end
  end

  describe 'admin can create new hull' do
    Given { login_as_admin }
    Given { visit new_hull_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('hull')

        check 'ATX'
        fill_in 'hull_power_supply_power', with: 9000
        select 'Horizontal'
        fill_in 'hull_number_of_storage_slots', with: 3
        fill_in 'hull_number_of_optical_drives', with: 1
        fill_in 'hull_number_of_front_usb', with: 4
        fill_in 'hull_video_card_max_length', with: 270
        fill_in 'hull_cpu_cooler_max_height', with: 135
        fill_in 'hull_power_supply_power', with: 9000
        check 'hull_front_usb3'
        check 'hull_front_headphone'
        check 'hull_front_mic'
        check 'hull_front_fire_wire'

        click_on 'Create Hull'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { Hull.last.power_supply_power.should == 9000 }
    end

    context 'parsing from Yandex Market' do
      When {initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=8454166&hid=91028', 'hull')}

      Then do
        find_field('hull[power_supply_power]').value.should eq '400'
        find_field('hull[power_supply_position]').value.should eq 'Horizontal'
        find_field('hull_form_factors_microatx').should be_checked
        find_field('hull[front_usb3]').should be_checked
        find_field('hull[front_headphone]').should be_checked
        find_field('hull[front_mic]').should be_checked
        find_field('hull[number_of_optical_drives]').value.should eq '2'
        find_field('hull[number_of_storage_slots]').value.should eq '2'
        find_field('hull[number_of_front_usb]').value.should eq '3'
        find_field('hull[cpu_cooler_max_height]').value.should eq '140'
        find_field('hull[video_card_max_length]').value.should eq '340'
      end
    end

    it_should_behave_like 'amazon fetchable', 'hull'
  end
end
