require 'spec_helper'

describe 'Video Cards' do

  describe 'index' do
    context 'without Video cards' do
      When { visit video_cards_path }
      it_should_behave_like 'absent items'
    end

    describe 'with items present' do
      When { visit video_cards_path}
      it_should_behave_like 'orderable list', :video_card
    end

    describe 'guide', js: true do
      Given { visit video_cards_path }
      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_video_card = create :video_card, :fitting
      @also_visible_video_card = create :video_card, :fitting
      @unnecessary_video_card = create :video_card, :fitting, number_of_6_pin: 1
    end

    context 'should offer options that have more than one possible value ' do
      When { visit video_cards_path }
      Then do
        within '#filters' do
          page.should have_content 'Necessary 6 pin connectors'
          page.should_not have_content 'Multi GPU technology'
        end
      end
    end

    context 'should display filtered Video cards' do
      When { visit video_cards_path }
      When  do
        within '#filters' do
          find('span', text: 'Necessary 6 pin connectors').click
          within('._with_number_of_6_pin') { check '2'}
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_number_of_6_pin_2').should be_checked
        page.all(".hardware-container").count.should eql(2)
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :video_card, :fitting }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new video card' do
    Given { login_as_admin }
    Given { visit new_video_card_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('video_card')

        select 'NVIDIA'
        fill_in 'video_card_chipset', with: 'NVIDIA Hodor'
        fill_in 'video_card_chipset_frequency', with: '3333'
        fill_in 'video_card_number_of_universal_processors', with: 9000
        fill_in 'video_card_memory_size', with: '4096'
        fill_in 'video_card_memory_frequency', with: '9000'
        select 'GDDR5'
        fill_in 'video_card_system_bus_bit', with: '1024'
        fill_in 'video_card_max_resolution', with: '4096x4096'
        fill_in 'video_card_number_of_supported_monitors', with: 4
        select 'SLI'
        fill_in 'video_card_number_of_6_pin', with: 2
        fill_in 'video_card_number_of_8_pin', with: 0



        click_on 'Create Video card'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { VideoCard.last.chipset.should == 'NVIDIA Hodor' }
    end

    context 'parsing from Yandex Market' do
      When {initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=10790131&hid=91031', 'video_card') }

      Then do
        find_field('video_card[chipset_manufacturer]').value.should eq 'AMD/ATI'
        find_field('video_card[chipset]').value.should eq 'AMD Radeon R9 295X2'
        find_field('video_card[chipset_frequency]').value.should eq '1018'
        find_field('video_card[number_of_supported_monitors]').value.should eq '6'
        find_field('video_card[number_of_universal_processors]').value.should eq '5632'
        find_field('video_card[memory_size]').value.should eq '8192'
        find_field('video_card[memory_type]').value.should eq 'GDDR5'
        find_field('video_card[memory_frequency]').value.should eq '5000'
        find_field('video_card[system_bus_bit]').value.should eq '1024'
        find_field('video_card[multi_gpu_technology]').value.should eq 'CrossFire'
        find_field('video_card[max_resolution]').value.should eq '4096x2160'
        find_field('video_card[number_of_6_pin]').value.should eq '0'
        find_field('video_card[number_of_8_pin]').value.should eq '2'
      end
    end

    it_should_behave_like 'amazon fetchable', 'video_card'
  end
end
