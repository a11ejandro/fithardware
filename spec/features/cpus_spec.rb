require 'spec_helper'

describe 'CPUs' do

  describe 'index' do
    context 'without CPUs' do
      When { visit cpus_path }
      it_should_behave_like 'absent items'
    end

    describe 'ordering' do
      When { visit cpus_path }

      it_should_behave_like 'orderable list', :cpu
    end

    describe 'guide', js: true do
      Given { visit cpus_path }

      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_cpu = create :cpu, :fitting
      @also_visible_cpu = create :cpu, :fitting
      @unnecessary_cpu = create :cpu, :fitting, integrated_gpu: 'Intel HD 4000'
    end

    context 'should offer options that have more than one possible value ' do
      When { visit cpus_path }
      Then do
        within '#filters' do
          page.should have_content 'Intel HD 4000'
          page.should_not have_content 'Socket'
        end
      end
    end

    context 'should display filtered CPUs' do
      When { visit cpus_path }
      When  do
        within '#filters' do
          find('span', text: 'Integrated GPU').click
          check 'Intel HD 4000'
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_integrated_gpu_intel_hd_4000').should be_checked
        page.all(".hardware-container").count.should eql(1)
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :cpu, :fitting }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new cpu' do
    Given { login_as_admin }
    Given { visit new_cpu_path }

    context 'with valid parameters' do
      When do
        fill_in 'cpu_amazon_price', with: 100500

        fill_in 'cpu_power_consumption', with: 9000
        fill_in 'cpu_product_name', with: "cpu MK23301"
        fill_in 'cpu_noise_level', with: 200
        fill_in 'cpu_description', with: 'Adeptus Mechanicus testing model MK 23301'

        fill_in 'Frequency', with: '2500'
        fill_in 'cpu_turbo_frequency', with: 2501
        fill_in 'Socket', with: 'LGA1'
        fill_in 'cpu_manufacturing_process', with: '22'
        fill_in 'cpu_architecture', with: 'IMP40000'
        fill_in 'cpu_integrated_gpu', with: 'Intel HD 4000'
        fill_in 'cpu_l2_cache_size', with: '2'
        fill_in 'cpu_l3_cache_size', with: '8'

        select 'Box'
        select 'AMD'

        click_on 'Create Cpu'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { Cpu.last.socket.should == 'LGA1' }
    end

    context 'fetch data from Amazon API' do
      When do
        fill_in "cpu_amazon_asin", with: 'B00CO8TBOW'
        click_on 'Fetch'
      end
      Then do
        find_field('Manufacturer').value.should eq 'Intel'
        find_field('Product name').value.should eq 'BX80646I54670K'
        find_field('Amazon link').value.should_not be_nil
        find_field('Price').value.should_not be_nil
        find_field('cpu_unpacked_weight').value.should == '0.33'
      end
    end

    context 'parsing from Yandex Market' do
      When { initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=10384313&hid=91019&CMD=-RR%3D0%2C0%2C0%2C0-VIS%3D8070-CAT_ID%3D651600-EXC%3D1-PG%3D10-PF%3D1801946~EQ~sel~5800975-PF%3D2142444694~EQ~sel~x1121255714', 'cpu')}

      Then do
        find_field('cpu[socket]').value.should eq 'LGA1150'
        find_field('cpu[number_of_cores]').value.should eq '4'
        find_field('cpu[l2_cache_size]').value.should eq '1024'
        find_field('cpu[l3_cache_size]').value.should eq '6144'
        find_field('cpu[architecture]').value.should eq 'Haswell'
        find_field('cpu[manufacturing_process]').value.should eq '22'
      end
    end
  end
end
