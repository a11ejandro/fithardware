require 'spec_helper'

describe 'HDDs' do

  describe 'index' do
    context 'without HDDs' do
      When { visit hdds_path }
      it_should_behave_like 'absent items'
    end

    describe 'with items present' do
      When { visit hdds_path}
      it_should_behave_like 'orderable list', :hdd
    end

    describe 'guide', js: true do
      Given { visit hdds_path }
      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_hdd = create :hdd, number_of_platters: 3, cache_size: 32
      @also_visible_hdd = create :hdd, number_of_platters: 3, cache_size: 64
      @unnecessary_hdd = create :hdd, number_of_platters: 3, cache_size: 128
    end

    context 'should offer options that have more than one possible value ' do
      When { visit hdds_path }
      Then do
        within '#filters' do
          page.should have_content 'Cache size, Mb'
          page.should_not have_content 'Number Of Platters'
        end
      end
    end

    context 'should display filtered hdds' do
      When { visit hdds_path }
      When  do
        within '#filters' do
          find('span', text: 'Cache size, Mb').click
          within('._with_cache_size') { check '64'}
          within('._with_cache_size') { check '32'}
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_cache_size_64').should be_checked
        find('#_with_cache_size_32').should be_checked
        page.all(".hardware-container").count.should eql(2)
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :hdd }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new hdd' do
    Given { login_as_admin }
    Given { visit new_hdd_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('hdd')

        fill_in 'hdd_capacity', with: 500
        fill_in 'hdd_cache_size', with: 128
        fill_in 'hdd_number_of_platters', with: 9000
        fill_in 'hdd_rpm', with: 7200
        check 'hdd_sata3_support'

        click_on 'Create Hdd'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { Hdd.last.number_of_platters.should == 9000 }
    end

    context 'parsing from Yandex Market' do
      When { initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=7773181&hid=91033', 'hdd')}

      Then do
        find_field('hdd[capacity]').value.should eq '1000'
        find_field('hdd[rpm]').value.should eq '7200'
        find_field('hdd[cache_size]').value.should eq '64'
        find_field('hdd[number_of_platters]').value.should eq '1'
        find_field('hdd[sata3_support]').should be_checked
        find_field('hdd[power_consumption]').value.should eq '8.0'
      end
    end

    it_should_behave_like 'amazon fetchable', 'hdd'
  end
end
