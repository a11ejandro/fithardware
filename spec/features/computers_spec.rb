require 'spec_helper'
describe 'Computers' do

  describe 'new computer' do
    Given { visit new_computer_path }
    context 'features', js: true do
      When do
        fill_in 'computer_description', with: 'Testing configuration'
        check 'computer_hackintosh_compatible'
        check 'computer_quiet'

        click_button 'Create'
      end

      Then do
        within '.features-inputs' do
          find_field('computer_description').value.should eq 'Testing configuration'
          page.has_checked_field?('computer_hackintosh_compatible').should be_true
          page.has_checked_field?('computer_quiet').should be_true
        end
      end
    end

    context 'hardware' do
      When { create_published_computer }
      Then { page.should have_content 'Successfully published' }
    end
  end

  describe 'show' do
    before do
      @published_computer = create :computer, :published
      @unpublished_computer = create :computer
    end

    context 'should display computer status' do
      When { visit computer_path(@unpublished_computer)}
      Then { page.should have_content 'Unverified'}
    end
  end
end

