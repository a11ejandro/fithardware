require 'spec_helper'

describe 'RAMs' do

  describe 'index' do
    context 'without RAMs' do
      When { visit rams_path }
      it_should_behave_like 'absent items'
    end

    describe 'ordering' do
      When { visit rams_path }
      it_should_behave_like 'orderable list', :ram
    end

    describe 'guide', js: true do
      Given { visit rams_path }

      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_ram = create :ram, :fitting
      @also_visible_ram = create :ram, :fitting
      @unnecessary_ram = create :ram, generation: 'DDR3'
    end

    context 'should offer options that have more than one possible value ' do
      When { visit rams_path }
      Then do
        within '#filters' do
          page.should have_content 'Frequency'
          page.should_not have_content 'Generation'
        end
      end
    end

    context 'should display filtered RAMs' do
      When { visit rams_path }
      When  do
        within '#filters' do
          find('span', text: 'Frequency, MHz').click
          check '1333'
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_frequency_1333').should be_checked
        page.all(".hardware-container").count.should eql(2)
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :ram, :fitting }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new ram' do
    Given { login_as_admin }
    Given { visit new_ram_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('ram')
        select 'DDR3'
        fill_in 'Capacity', with: '4096'
        fill_in 'Frequency', with: '444'

        click_on 'Create Ram'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { Ram.last.frequency.should == 444 }
    end

    context 'parsing from Yandex Market' do
      When {initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=8441906&hid=191211', 'ram')}

      Then do
        find_field('ram[capacity]').value.should eq '4'
        find_field('ram[frequency]').value.should eq '2400'
        find_field('ram[generation]').value.should eq 'DDR3'
        find_field('ram[items_in_pack]').value.should eq '2'
      end
    end


    it_should_behave_like 'amazon fetchable', 'ram'
  end
end