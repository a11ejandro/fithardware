require 'spec_helper'

describe 'Motherboards' do

  describe 'index' do
    context 'without motherboards' do
      When { visit motherboards_path }
      it_should_behave_like 'absent items'
    end

    describe 'ordering' do
      When { visit motherboards_path }
      it_should_behave_like 'orderable list', :motherboard
    end

    describe 'guide', js: true do
      Given { visit motherboards_path }

      it_should_behave_like 'showable guide'
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :motherboard, :fitting }

    it_should_behave_like 'addable hardware'
  end

  describe 'filtering' do
    Given do
      @necessary_motherboard = create :motherboard, :fitting
      @also_visible_motherboard = create :motherboard, :fitting
      @unnecessary_motherboard = create :motherboard, ram_generations: ['DDR3']
    end

    context 'should offer options that have more than one possible value ' do
      When { visit motherboards_path }
      Then do
        within '#filters' do
          page.should have_content 'Socket'
          page.should_not have_content 'RAM Generation'
        end
      end
    end

    context 'should display filtered motherboards' do
      When { visit motherboards_path }
      When  do
        within '#filters' do
          find('span', text: 'Socket').click
          check 'Fitting Socket'
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_socket_fitting_socket').should be_checked
        page.all(".hardware-container").count.should eql(2)
      end
    end
  end

  describe 'admin can create new motherboard' do
    Given { login_as_admin }
    Given { visit new_motherboard_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('motherboard')
        fill_in 'motherboard_chipset', with: 'B55'
        fill_in 'motherboard_socket', with: 'LGA 1356'
        fill_in 'motherboard_number_of_sockets', with: '1'
        fill_in 'motherboard_number_of_sata2_ports', with: '5'
        fill_in 'motherboard_number_of_sata3_ports', with: '0'
        fill_in 'motherboard_number_of_ram_slots', with: '4'
        fill_in 'motherboard_number_of_pcie_x16_slots', with: '1'
        fill_in 'motherboard_number_of_pcie_x1_slots', with: '2'
        fill_in 'motherboard_number_of_pcie_x8_slots', with: '3'

        select 'ATX'
        check 'motherboard_ram_generations_ddr3'
        check 'motherboard_supported_multi_gpus_crossfire'

        click_on 'Create Motherboard'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { Motherboard.last.socket.should == 'LGA 1356' }
    end

    context 'parsing from Yandex Market' do
      When {initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=10530720&hid=91020', 'motherboard') }

      Then do
        find_field('motherboard[form_factor]').value.should eq 'microATX'
        find_field('motherboard[chipset]').value.should eq 'AMD A88X'
        find_field('motherboard[socket]').value.should eq 'FM2+'
        find_field('motherboard[number_of_sockets]').value.should eq '1'
        find_field('motherboard[max_ram_total_size]').value.should eq '64'
        find_field('motherboard_ram_generations_ddr3').should be_checked
        find_field('motherboard[number_of_ram_slots]').value.should eq '2'
        find_field('motherboard[max_ram_frequency]').value.should eq '2133'
        find_field('motherboard_supported_multi_gpus_crossfire').should be_checked
        find_field('motherboard[integrated_audio]').value.should eq 'Realtek ALC887'
        find_field('motherboard[number_of_pcie_x16_slots]').value.should eq '2'
        find_field('motherboard[number_of_pcie_x1_slots]').value.should eq '1'
        find_field('motherboard[number_of_sata2_ports]').value.should eq '0'
        find_field('motherboard[number_of_sata3_ports]').value.should eq '8'
        find_field('motherboard[number_of_usb_ports]').value.should eq '12'
        find_field('motherboard[number_of_usb3_ports]').value.should eq '4'
      end
    end

    it_should_behave_like 'amazon fetchable', 'motherboard'
  end
end