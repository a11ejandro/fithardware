require 'spec_helper'

describe 'CPU coolers' do

  describe 'index' do
    context 'without CPU coolers' do
      When { visit cpu_coolers_path }
      it_should_behave_like 'absent items'
    end

    describe 'with items present' do
      When { visit cpu_coolers_path}
      it_should_behave_like 'orderable list', :cpu_cooler
    end

    describe 'guide', js: true do
      Given { visit cpu_coolers_path }
      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_cooler = create :cpu_cooler, :fitting
      @also_visible_cooler = create :cpu_cooler, :fitting
      @unnecessary_cooler = create :cpu_cooler, :fitting, form_factor: 'TOP'
    end

    context 'should offer options that have more than one possible value ' do
      When { visit cpu_coolers_path }
      Then do
        within '#filters' do
          page.should have_content 'TOP'
          page.should_not have_content 'Sockets'
        end
      end
    end

    context 'should display filtered CPU coolers' do
      When { visit cpu_coolers_path }
      When  do
        within '#filters' do
          find('span', text: 'Form Factor').click
          check 'Tower'
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_form_factor_tower').should be_checked
        page.all(".hardware-container").count.should eql(2)
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :cpu_cooler, :fitting }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new cpu cooler' do
    Given { login_as_admin }
    Given { visit new_cpu_cooler_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('cpu_cooler')

        fill_in 'cpu_cooler_min_rotation_speed', with: '600'
        fill_in 'cpu_cooler_max_rotation_speed', with: '1200'
        fill_in 'Sockets', with: 'LGA1, LGA3'
        select 'Tower'

        click_on 'Create Cpu cooler'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { CpuCooler.last.sockets.should == ['LGA1', 'LGA3'] }
    end

    context 'parsing from Yandex Market' do
      When { initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=9275061&hid=818965', 'cpu_cooler') }

      Then do
        find_field('cpu_cooler[sockets]').value.should eq '["S1150", "1155", "S1156", "S1356", "S1366", "S2011", "AM2", "AM2+", "AM3", "AM3+", "FM1", "FM2"]'
        find_field('cpu_cooler[form_factor]').value.should eq 'Liquid'
        find_field('cpu_cooler[min_rotation_speed]').value.should eq '900'
        find_field('cpu_cooler[max_rotation_speed]').value.should eq  '2000'
      end
    end

    it_should_behave_like 'amazon fetchable', 'cpu_cooler'
  end
end
