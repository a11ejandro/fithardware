require 'spec_helper'

describe 'Power Supplies' do

  describe 'index' do
    context 'without Video cards' do
      When { visit power_supplies_path }
      it_should_behave_like 'absent items'
    end

    describe 'with items present' do
      When { visit power_supplies_path}
      it_should_behave_like 'orderable list', :power_supply
    end

    describe 'guide', js: true do
      Given { visit power_supplies_path }
      it_should_behave_like 'showable guide'
    end
  end

  describe 'filtering' do
    Given do
      @necessary_power_supply = create :power_supply, :fitting
      @also_visible_power_supply = create :power_supply, :fitting
      @unnecessary_power_supply = create :power_supply, :fitting, number_of_6_pin: 1
    end

    context 'should offer options that have more than one possible value ' do
      When { visit power_supplies_path }
      Then do
        within '#filters' do
          page.should have_content '6 pin connectors'
          page.should_not have_content 'Multi GPU technology'
        end
      end
    end

    context 'should display filtered power supplies' do
      When { visit power_supplies_path }
      When  do
        within '#filters' do
          find('span', text: '6 pin connectors').click
          within('._with_number_of_6_pin') { check '4'}
          click_button 'Filter'
        end
      end
      Then do
        find('#_with_number_of_6_pin_4').should be_checked
        page.all(".hardware-container").count.should eql(2)
      end
    end
  end

  describe 'Adding and removing from computer' do
    Given { @computer = create :computer }
    Given { @hardware = create :power_supply, :fitting }

    it_should_behave_like 'addable hardware'
  end

  describe 'admin can create new power supply' do
    Given { login_as_admin }
    Given { visit new_power_supply_path }

    context 'with valid parameters' do
      When do
        fill_in_common_fields('power_supply')

        select 'Gold'
        fill_in 'power_supply_number_of_6_pin', with: 3
        fill_in 'power_supply_number_of_8_pin', with: 14
        fill_in 'power_supply_number_of_6_2_pin', with: 15
        fill_in 'power_supply_number_of_sata_connectors', with: 9
        fill_in 'power_supply_power', with: 9000
        check 'power_supply_modular_cables'

        click_on 'Create Power supply'
      end

      Then { page.should_not have_selector '#error_explanation'}
      And { PowerSupply.last.power.should == 9000 }
    end

    context 'parsing from Yandex Market' do
      When {initialize_ym_parsing('http://market.yandex.ua/model-spec.xml?modelid=4698558&hid=857707', 'power_supply')}

      Then do
        find_field('power_supply[power]').value.should eq '900'
        find_field('power_supply[number_of_6_pin]').value.should eq '2'
        find_field('power_supply[number_of_8_pin]').value.should eq '2'
        find_field('power_supply[number_of_sata_connectors]').value.should eq '8'
        find_field('power_supply[certificate_80_plus]').value.should eq 'none'
        find_field('power_supply[noise_level]').value.should eq '21'
      end
    end

    it_should_behave_like 'amazon fetchable', 'power_supply'
  end
end
