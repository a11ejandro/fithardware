require 'builder'

module ConversionCoefficients
  HUNDREDTHS_POUNDS_TO_KG = 0.0045
  POUNDS_TO_KG = 0.45
  GRAMMS_TO_KG = 0.001
  HUNDREDTHS_INCHES_TO_MM = 0.254
  INCHES_TO_MM = 25.4
  METERS_TO_MM  = 1000

  COEFFICIENTS = {
      'hundredths-pounds' => HUNDREDTHS_POUNDS_TO_KG,
      'pounds'            => POUNDS_TO_KG,
      'gramms'            => GRAMMS_TO_KG,
      'inches'            => INCHES_TO_MM,
      'hundredths-inches' => HUNDREDTHS_INCHES_TO_MM,
      'meters'            => METERS_TO_MM
  }
end