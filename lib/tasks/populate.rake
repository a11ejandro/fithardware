namespace :db do
  desc "Populate db with random data"
  task :populate => :environment do
    20.times do
      FactoryGirl.create :hull
      FactoryGirl.create :motherboard
      FactoryGirl.create :cpu
      FactoryGirl.create :ram
      FactoryGirl.create :ssd
      FactoryGirl.create :hdd
      FactoryGirl.create :optical_drive
      FactoryGirl.create :video_card
      FactoryGirl.create :power_supply
      FactoryGirl.create :cpu_cooler
    end

    10.times do
      [:hull, :motherboard, :cpu, :ram, :video_card, :power_supply, :cpu_cooler].each {|hw| FactoryGirl.create hw, :fitting}
    end

    5.times do
      FactoryGirl.create :hull, power_supply_power: rand(300..800)
    end

    puts "Hardware creation complete."

    5.times { FactoryGirl.create :user}
    FactoryGirl.create :user, :admin
    puts "Users creation complete."
  end
end
