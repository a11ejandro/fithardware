namespace :whenever do
  desc 'Removes unpublished computers without any user'
  task :remove_orphan_computers => :environment do
    Computer.where(:state.ne => 'published').and(:users => nil).delete
  end

  desc 'Removes old computers without any user'
  task :remove_old_computers => :environment do
    restriction_period = Time.now - 6.months
    Computer.where(:updated_at.lte => restriction_period).and(:users => nil).delete
  end
end