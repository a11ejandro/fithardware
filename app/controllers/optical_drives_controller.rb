class OpticalDrivesController < HardwareController
  # For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_optical_drive, :only => [:new, :create]

  before_action :set_optical_drive, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:cd_r_write, :cd_rw_write, :dvd_read, :dvd_r_write, :dvd_rw_write, :dvd_dl_write, :bd_read,
   :bd_r_write, :bd_re_write, :bd_dl_write].each do |field_symbol|
    has_scope "with_#{field_symbol}".to_sym, using: [:min, :max], type: :hash, only: :filter
  end

  # GET /optical_drives
  # GET /optical_drives.json
  def index
    @optical_drives = apply_scopes(@optical_drives).all.page(params[:page])
    @filter_values = @optical_drives.filter_values
  end

  def filter
    @filter_values = @optical_drives.filter_values
    @optical_drives = apply_scopes(@optical_drives).all.page(params[:page])
    render :index
  end

  # GET /optical_drives/1
  # GET /optical_drives/1.json
  def show
  end

  # GET /optical_drives/new
  def new
    @optical_drive = OpticalDrive.new
  end

  # GET /optical_drives/1/edit
  def edit
  end

  # POST /optical_drives
  # POST /optical_drives.json
  def create
    @optical_drive = OpticalDrive.new(optical_drive_params)

    respond_to do |format|
      if @optical_drive.save
        format.html { redirect_to optical_drives_path, notice: 'Optical Drive was successfully created.' }
        format.json { render action: 'show', status: :created, location: @optical_drive }
      else
        format.html { render action: 'new' }
        format.json { render json: @optical_drive.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /optical_drives/1
  # PATCH/PUT /optical_drives/1.json
  def update
    respond_to do |format|
      if @optical_drive.update(optical_drive_params)
        format.html { redirect_to optical_drives_path, notice: 'Optical Drive was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @optical_drive.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /optical_drives/1
  # DELETE /optical_drives/1.json
  def destroy
    @optical_drive.destroy
    respond_to do |format|
      format.html { redirect_to optical_drives_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.optical_drives << @optical_drive
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add Optical Drive to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.optical_drives.remove(@optical_drive)
    render 'index' unless @computer.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't delete Optical Drive from configuration"
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_optical_drive
    @optical_drive = OpticalDrive.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def optical_drive_params
    params.require(:optical_drive).permit(HardwareController::PERMITTED_PARAMS + [:cd_r_write, :cd_rw_write, :dvd_read,
                          :dvd_r_write, :dvd_rw_write, :dvd_dl_write, :bd_read, :bd_r_write, :bd_re_write, :bd_dl_write])
  end


  def consider_computer
    @optical_drives = OpticalDrive.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_optical_drive
    @optical_drive = OpticalDrive.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @optical_drive = OpticalDrive.find(params[:id]) if params[:id]
      @optical_drive ||= OpticalDrive.new(optical_drive_params)
      @optical_drive.parse_from_services
      render :edit
    end
  end
end

