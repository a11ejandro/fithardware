class CpusController < HardwareController
  # For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_cpu, :only => [:new, :create]

  before_action :set_cpu, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:socket, :integrated_gpu, :number_of_cores, :manufacturing_process, :architecture, :l2_cache_size, :l3_cache_size,
   :complectation].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  [:frequency, :turbo_frequency].each do |field_symbol|
    has_scope "with_#{field_symbol}".to_sym, using: [:min, :max], type: :hash, only: :filter
  end

  has_scope :with_integrated_gpu_present, type: :boolean, only: :filter

  # GET /cpus
  # GET /cpus.json
  def index
    @cpus = apply_scopes(@cpus).all.page(params[:page])
    @filter_values = @cpus.filter_values
  end

  def filter
    @filter_values = @cpus.filter_values
    @cpus = apply_scopes(@cpus).all.page(params[:page])
    render :index
  end

  # GET /cpus/1
  # GET /cpus/1.json
  def show
  end

  # GET /cpus/new
  def new
    @cpu = Cpu.new
  end

  # GET /cpus/1/edit
  def edit
  end

  # POST /cpus
  # POST /cpus.json
  def create
    @cpu = Cpu.new(cpu_params)

    respond_to do |format|
      if @cpu.save
        format.html { redirect_to cpus_path, notice: 'Cpu was successfully created.' }
        format.json { render action: 'show', status: :created, location: @cpu }
      else
        format.html { render action: 'new' }
        format.json { render json: @cpu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cpus/1
  # PATCH/PUT /cpus/1.json
  def update
    respond_to do |format|
      if @cpu.update(cpu_params)
        format.html { redirect_to cpus_path, notice: 'Cpu was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cpu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cpus/1
  # DELETE /cpus/1.json
  def destroy
    @cpu.destroy
    respond_to do |format|
      format.html { redirect_to cpus_path }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.cpus << @cpu
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add CPU to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.cpus.remove(@cpu)
    render 'index' unless @computer.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't delete CPU from configuration"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cpu
      @cpu = Cpu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cpu_params
      params.require(:cpu).permit(HardwareController::PERMITTED_PARAMS + [:socket, :integrated_gpu, :frequency,
                                   :turbo_frequency, :architecture, :manufacturing_process, :number_of_cores,
                                   :l2_cache_size, :l3_cache_size, :complectation])
    end


  def consider_computer
    @cpus = Cpu.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_cpu
    @cpu = Cpu.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @cpu = Cpu.find(params[:id]) if params[:id]

      @cpu ||= Cpu.new(cpu_params)
      @cpu.parse_from_services
      render :edit
    end
  end
end
