class HddsController < HardwareController
  # For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_hdd, :only => [:new, :create]

  before_action :set_hdd, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:number_of_platters, :rpm, :cache_size].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  has_scope :with_sata3_support, type: :boolean, only: :filter
  has_scope :with_capacity, using: [:min, :max], type: :hash, only: :filter

  # GET /hdds
  # GET /hdds.json
  def index
    @hdds = apply_scopes(@hdds).all.page(params[:page])
    @filter_values = @hdds.filter_values
  end

  def filter
    @filter_values = @hdds.filter_values
    @hdds = apply_scopes(@hdds).all.page(params[:page])
    render :index
  end

  # GET /hdds/1
  # GET /hdds/1.json
  def show
  end

  # GET /hdds/new
  def new
    @hdd = Hdd.new
  end

  # GET /hdds/1/edit
  def edit
  end

  # POST /hdds
  # POST /hdds.json
  def create
    @hdd = Hdd.new(hdd_params)

    respond_to do |format|
      if @hdd.save
        format.html { redirect_to hdds_path, notice: 'Hdd was successfully created.' }
        format.json { render action: 'show', status: :created, location: @hdd }
      else
        format.html { render action: 'new' }
        format.json { render json: @hdd.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hdds/1
  # PATCH/PUT /hdds/1.json
  def update
    respond_to do |format|
      if @hdd.update(hdd_params)
        format.html { redirect_to hdds_path, notice: 'Hdd was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @hdd.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hdds/1
  # DELETE /hdds/1.json
  def destroy
    @hdd.destroy
    respond_to do |format|
      format.html { redirect_to hdds_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.hdds << @hdd
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add HDD to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.hdds.remove(@hdd)
    render 'index' unless @computer.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't delete HDD from configuration"
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_hdd
    @hdd = Hdd.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def hdd_params
    params.require(:hdd).permit(HardwareController::PERMITTED_PARAMS + [:number_of_platters, :capacity, :cache_size, :rpm,
                                                                        :sata3_support])
  end


  def consider_computer
    @hdds = Hdd.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_hdd
    @hdd = Hdd.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @hdd = Hdd.find(params[:id]) if params[:id]
      @hdd ||= Hdd.new(hdd_params)
      @hdd.parse_from_services
      render :edit
    end
  end
end
