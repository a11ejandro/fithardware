class ComputersController < ApplicationController
  before_action :set_configuration, only: [:show, :edit, :update, :destroy, :publish]

  # GET /computers
  # GET /computers.json
  def index
    @computers = Computer.all
    @computers = @computers.where(:user => params[:user_id]) if params[:user_id]
    @computers = @computers.page(params[:page])
  end

  # GET /computers/1
  # GET /computers/1.json
  def show
  end

  # GET /computers/new
  def new
    @computer = Computer.new
    @computer.user = current_user
  end

  # GET /computers/1/edit
  def edit
  end

  # POST /computers
  # POST /computers.json
  def create
    @computer = Computer.new(computer_params)

    respond_to do |format|
      if verify_recaptcha(model: @computer) && @computer.save!
        format.html { redirect_to edit_computer_path(@computer), notice: 'Computer was successfully created.' }
        format.json { render action: 'show', status: :created, location: @computer }
      else
        format.html { render action: 'new' }
        format.json { render json: @computer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /computers/1
  # PATCH/PUT /computers/1.json
  def update
    respond_to do |format|
      if @computer.update_attributes(computer_params)
        format.html { render action: 'edit', notice: 'Computer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @computer.errors, status: :unprocessable_entity }
      end
    end
  end

  def publish
    @computer.publish
    if @computer.save && @computer.published?
      flash[:notice] = 'Successfully published'
      render 'show'
    else
      render 'edit'
    end
  end

  # DELETE /computers/1
  # DELETE /computers/1.json
  def destroy
    @computer.destroy
    respond_to do |format|
      format.html { redirect_to computers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_configuration
      @computer = Computer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def computer_params
      params.require(:computer).permit([:description, :hackintosh_compatible, :quiet])
    end
end
