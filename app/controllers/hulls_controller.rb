class HullsController < HardwareController
  # For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_hull, :only => [:new, :create]
  before_action :reject_empty_params, only: [:create, :update]

  before_action :set_hull, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:form_factors, :power_supply_position, :number_of_storage_slots, :number_of_optical_drives,
   :number_of_front_usb].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  [:power_supply_power, :video_card_max_length, :cpu_cooler_max_height].each do |field_symbol|
    has_scope "with_#{field_symbol}".to_sym, using: [:min, :max], type: :hash, only: :filter
  end

  [:front_usb3, :front_mic, :front_headphone, :front_fire_wire].each do |field_symbol|
    has_scope "with_#{field_symbol}".to_sym, type: :boolean, only: :filter
  end


  # GET /hulls
  # GET /hulls.json
  def index
    @hulls = apply_scopes(@hulls).all.page(params[:page])
    @filter_values = @hulls.filter_values
  end

  def filter
    @filter_values = @hulls.filter_values
    @hulls = apply_scopes(@hulls).all.page(params[:page])
    render :index
  end

  # GET /hulls/1
  # GET /hulls/1.json
  def show
  end

  # GET /hulls/new
  def new
    @hull = Hull.new
  end

  # GET /hulls/1/edit
  def edit
  end

  # POST /hulls
  # POST /hulls.json
  def create
    @hull = Hull.new(hull_params)

    respond_to do |format|
      if @hull.save
        format.html { redirect_to hulls_path, notice: 'Hull was successfully created.' }
        format.json { render action: 'show', status: :created, location: @hull }
      else
        format.html { render action: 'new' }
        format.json { render json: @hull.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hulls/1
  # PATCH/PUT /hulls/1.json
  def update
    respond_to do |format|
      if @hull.update(hull_params)
        format.html { redirect_to hulls_path, notice: 'Hull was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @hull.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hulls/1
  # DELETE /hulls/1.json
  def destroy
    @hull.destroy
    respond_to do |format|
      format.html { redirect_to hulls_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.hull = @hull
    @computer.power_supply = @hull.power_supply if @hull.power_supply.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add Hull to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.power_supply = nil if @computer.power_supply == @hull.power_supply
    @computer.hull = nil
    @computer.save
    redirect_to edit_computer_url(@computer)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_hull
    @hull = Hull.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def hull_params
    params.require(:hull).permit(HardwareController::PERMITTED_PARAMS + [:power_supply_power, :power_supply_position,
                                 :number_of_storage_slots, :number_of_optical_drives, :number_of_front_usb, :front_usb3,
                                 :front_mic, :front_headphone, :front_fire_wire, :video_card_max_length,
                                 :cpu_cooler_max_height, form_factors: []])
  end

  def reject_empty_params
    params[:hull][:form_factors].reject!(&:blank?) if params[:hull][:form_factors].present?
  end

  def consider_computer
    @hulls = Hull.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_hull
    @hull = Hull.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @hull = Hull.find(params[:id]) if params[:id]
      @hull ||= Hull.new(hull_params)
      @hull.parse_from_services
      render :edit
    end
  end
end
