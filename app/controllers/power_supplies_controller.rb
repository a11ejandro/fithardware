class PowerSuppliesController < HardwareController
  # For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_power_supply, :only => [:new, :create]

  before_action :set_power_supply, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:number_of_6_pin, :number_of_8_pin, :number_of_6_2_pin, :number_of_sata_connectors, :certificate_80_plus].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  has_scope :with_modular_cables, type: :boolean, only: :filter
  has_scope :with_power, using: [:min, :max], type: :hash, only: :filter


  # GET /power_supplies
  # GET /power_supplies.json
  def index
    @power_supplies = apply_scopes(@power_supplies).all.page(params[:page])
    @filter_values = @power_supplies.filter_values
  end

  def filter
    @filter_values = @power_supplies.filter_values
    @power_supplies = apply_scopes(@power_supplies).all.page(params[:page])
    render :index
  end

  # GET /power_supplies/1
  # GET /power_supplies/1.json
  def show
  end

  # GET /power_supplies/new
  def new
    @power_supply = PowerSupply.new
  end

  # GET /power_supplies/1/edit
  def edit
  end

  # POST /power_supplies
  # POST /power_supplies.json
  def create
    @power_supply = PowerSupply.new(power_supply_params)

    respond_to do |format|
      if @power_supply.save
        format.html { redirect_to power_supplies_path, notice: 'Power Supply was successfully created.' }
        format.json { render action: 'show', status: :created, location: @power_supply }
      else
        format.html { render action: 'new' }
        format.json { render json: @power_supply.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /power_supplies/1
  # PATCH/PUT /power_supplies/1.json
  def update
    respond_to do |format|
      if @power_supply.update(power_supply_params)
        format.html { redirect_to power_supplies_path, notice: 'Power Supply was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @power_supply.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /power_supplies/1
  # DELETE /power_supplies/1.json
  def destroy
    @power_supply.destroy
    respond_to do |format|
      format.html { redirect_to power_supplies_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.power_supply = @power_supply
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add Power Supply to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.power_supply = nil
    @computer.save
    redirect_to edit_computer_url(@computer)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_power_supply
    @power_supply = PowerSupply.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def power_supply_params
    params.require(:power_supply).permit(HardwareController::PERMITTED_PARAMS + [:number_of_6_pin, :number_of_8_pin, :number_of_6_2_pin,
                                          :number_of_sata_connectors, :modular_cables, :certificate_80_plus, :power])
  end


  def consider_computer
    @power_supplies = PowerSupply.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_power_supply
    @power_supply = PowerSupply.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @power_supply = PowerSupply.find(params[:id]) if params[:id]
      @power_supply ||= PowerSupply.new(power_supply_params)
      @power_supply.parse_from_services
      render :edit
    end
  end
end
