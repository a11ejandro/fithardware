﻿class CpuCoolersController < HardwareController
# For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_cpu_cooler, :only => [:new, :create]

  before_action :extract_sockets, only: [:create, :update]
  before_action :set_cpu_cooler, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:manufacturer, :sockets, :form_factor].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  [:min_rotation_speed, :max_rotation_speed].each do |field_symbol|
    has_scope "with_#{field_symbol}".to_sym, using: [:min, :max], type: :hash, only: :filter
  end

  # GET /cpu_coolers
  # GET /cpu_coolers.json
  def index
    @cpu_coolers = apply_scopes(@cpu_coolers).all.page(params[:page])
    @filter_values = @cpu_coolers.filter_values
  end

  def filter
    @filter_values = @cpu_coolers.filter_values
    @cpu_coolers = apply_scopes(@cpu_coolers).all.page(params[:page])
    render :index
  end

  # GET /cpu_coolers/1
  # GET /cpu_coolers/1.json
  def show
  end

  # GET /cpu_coolers/new
  def new
    @cpu_cooler = CpuCooler.new
  end

  # GET /cpu_coolers/1/edit
  def edit
  end

  # POST /cpu_coolers
  # POST /cpu_coolers.json
  def create
    @cpu_cooler = CpuCooler.new(cpu_cooler_params)

    respond_to do |format|
      if @cpu_cooler.save
        format.html { redirect_to cpu_coolers_path, notice: 'Cpu Cooler was successfully created.' }
        format.json { render action: 'show', status: :created, location: @cpu_cooler }
      else
        format.html { render action: 'new' }
        format.json { render json: @cpu_cooler.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cpu_coolers/1
  # PATCH/PUT /cpu_coolers/1.json
  def update
    respond_to do |format|
      if @cpu_cooler.update(cpu_cooler_params)
        format.html { redirect_to cpu_coolers_path, notice: 'Cpu Cooler was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cpu_cooler.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cpu_coolers/1
  # DELETE /cpu_coolers/1.json
  def destroy
    @cpu_cooler.destroy
    respond_to do |format|
      format.html { redirect_to cpu_coolers_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.cpu_coolers << @cpu_cooler
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add CPU Cooler to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.cpu_coolers.remove(@cpu_cooler)
    render 'index' unless @computer.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't delete CPU Cooler from configuration"
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cpu_cooler
    @cpu_cooler = CpuCooler.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cpu_cooler_params
    params.require(:cpu_cooler).permit(HardwareController::PERMITTED_PARAMS + [:min_rotation_speed, :max_rotation_speed,
                                                                               :form_factor, sockets: []])
  end


  def consider_computer
    @cpu_coolers = CpuCooler.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_cpu_cooler
    @cpu_cooler = CpuCooler.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @cpu_cooler = CpuCooler.find(params[:id]) if params[:id]
      @cpu_cooler ||= CpuCooler.new(cpu_cooler_params)
      @cpu_cooler.parse_from_services
      render :edit
    end
  end

  def extract_sockets
    params[:cpu_cooler][:sockets] = params[:cpu_cooler][:sockets].split(', ') if params[:cpu_cooler][:sockets].is_a? String
  end
end
