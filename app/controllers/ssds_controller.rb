class SsdsController < HardwareController
  # For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_ssd, :only => [:new, :create]

  before_action :set_ssd, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:capacity, :read_speed, :write_speed, :random_write_speed].each do |field_symbol|
    has_scope "with_#{field_symbol}".to_sym, using: [:min, :max], type: :hash, only: :filter
  end

  has_scope :with_sata3_support, type: :boolean, only: :filter

  # GET /ssds
  # GET /ssds.json
  def index
    @ssds = apply_scopes(@ssds).all.page(params[:page])
    @filter_values = @ssds.filter_values
  end

  def filter
    @filter_values = @ssds.filter_values
    @ssds = apply_scopes(@ssds).all.page(params[:page])
    render :index
  end

  # GET /ssds/1
  # GET /ssds/1.json
  def show
  end

  # GET /ssds/new
  def new
    @ssd = Ssd.new
  end

  # GET /ssds/1/edit
  def edit
  end

  # POST /ssds
  # POST /ssds.json
  def create
    @ssd = Ssd.new(ssd_params)

    respond_to do |format|
      if @ssd.save
        format.html { redirect_to ssds_path, notice: 'Ssd was successfully created.' }
        format.json { render action: 'show', status: :created, location: @ssd }
      else
        format.html { render action: 'new' }
        format.json { render json: @ssd.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ssds/1
  # PATCH/PUT /ssds/1.json
  def update
    respond_to do |format|
      if @ssd.update(ssd_params)
        format.html { redirect_to ssds_path, notice: 'Ssd was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ssd.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ssds/1
  # DELETE /ssds/1.json
  def destroy
    @ssd.destroy
    respond_to do |format|
      format.html { redirect_to ssds_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.ssds << @ssd
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add SSD to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.ssds.remove(@ssd)
    render 'index' unless @computer.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't delete SSD from configuration"
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_ssd
    @ssd = Ssd.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def ssd_params
    params.require(:ssd).permit(HardwareController::PERMITTED_PARAMS + [:capacity, :read_speed, :write_speed,
                                                                        :random_write_speed, :sata3_support])
  end


  def consider_computer
    @ssds = Ssd.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_ssd
    @ssd = Ssd.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @ssd = Ssd.find(params[:id]) if params[:id]
      @ssd ||= Ssd.new(ssd_params)
      @ssd.parse_from_services
      render :edit
    end
  end
end
