class HardwareController < ApplicationController
  before_filter :parse_from_services, only: [:create, :update]

  PERMITTED_PARAMS = [:image, :remote_image_url, :image_cache, :amazon_asin, :packed_weight, :unpacked_weight, :packed_height,
                      :packed_width, :packed_length, :unpacked_height, :unpacked_width, :items_in_pack,
                      :unpacked_length, :amazon_link, :amazon_price, :yandex_market_link, :manufacturer, :product_name,
                      :description, :hackintosh_recommended, :power_consumption, :performance_score, :noise_level ]

  [:by_price_asc, :by_price_desc, :by_creation_time_asc, :by_creation_time_desc].each do |scope_symbol|
    has_scope scope_symbol, type: :boolean, only: [:index, :filter]
  end

  has_scope :with_amazon_price, using: [:min, :max], type: :hash, only: :filter
  has_scope :with_manufacturer, type: :array, only: :filter do |controller, scope, values|
    (values.all? &:blank?) ? scope : scope.send(:with_manufacturer, values)
  end

end