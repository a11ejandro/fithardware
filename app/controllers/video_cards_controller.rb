class VideoCardsController < HardwareController
# For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_video_card, :only => [:new, :create]

  before_action :set_video_card, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:chipset, :chipset_manufacturer, :memory_type, :number_of_6_pin, :number_of_8_pin, :system_bus_bit,
   :number_of_supported_monitors, :max_resolution, :multi_gpu_technology].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  [:chipset_frequency, :memory_size, :memory_frequency, :number_of_universal_processors].each do |field_symbol|
    has_scope "with_#{field_symbol}".to_sym, using: [:min, :max], type: :hash, only: :filter
  end


  # GET /video_cards
  # GET /video_cards.json
  def index
    @video_cards = apply_scopes(@video_cards).all.page(params[:page])
    @filter_values = @video_cards.filter_values
  end

  def filter
    @filter_values = @video_cards.filter_values
    @video_cards = apply_scopes(@video_cards).all.page(params[:page])
    render :index
  end

  # GET /video_cards/1
  # GET /video_cards/1.json
  def show
  end

  # GET /video_cards/new
  def new
    @video_card = VideoCard.new
  end

  # GET /video_cards/1/edit
  def edit
  end

  # POST /video_cards
  # POST /video_cards.json
  def create
    @video_card = VideoCard.new(video_card_params)

    respond_to do |format|
      if @video_card.save
        format.html { redirect_to video_cards_path, notice: 'Video Card was successfully created.' }
        format.json { render action: 'show', status: :created, location: @video_card }
      else
        format.html { render action: 'new' }
        format.json { render json: @video_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /video_cards/1
  # PATCH/PUT /video_cards/1.json
  def update
    respond_to do |format|
      if @video_card.update(video_card_params)
        format.html { redirect_to video_cards_path, notice: 'Video Card was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @video_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /video_cards/1
  # DELETE /video_cards/1.json
  def destroy
    @video_card.destroy
    respond_to do |format|
      format.html { redirect_to video_cards_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.video_cards << @video_card
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add Video Card to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.video_cards.remove(@video_card)
    render 'index' unless @computer.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't delete Video Card from configuration"
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_video_card
    @video_card = VideoCard.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def video_card_params
    params.require(:video_card).permit(HardwareController::PERMITTED_PARAMS + [:chipset, :chipset_manufacturer, :memory_type,
      :number_of_6_pin, :number_of_8_pin, :system_bus_bit, :number_of_supported_monitors, :max_resolution,
      :multi_gpu_technology, :chipset_frequency, :memory_size, :memory_frequency, :number_of_universal_processors])
  end


  def consider_computer
    @video_cards = VideoCard.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_video_card
    @video_card = VideoCard.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @video_card = VideoCard.find(params[:id]) if params[:id]
      @video_card ||= VideoCard.new(video_card_params)
      @video_card.parse_from_services
      render :edit
    end
  end
end

