class RamsController < HardwareController
  # For compatibility with CanCan we must manually set hardware to prevent triggering of it's getter
  before_filter :new_ram, :only => [:new, :create]

  before_action :set_ram, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:capacity, :generation, :frequency].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  # GET /rams
  # GET /rams.json
  def index
    @rams = apply_scopes(@rams).all.page(params[:page])
    @filter_values = @rams.filter_values
  end

  def filter
    @filter_values = @rams.filter_values
    @rams = apply_scopes(@rams).all.page(params[:page])
    render :index
  end

  # GET /rams/1
  # GET /rams/1.json
  def show
  end

  # GET /rams/new
  def new
    @ram = Ram.new
  end

  # GET /rams/1/edit
  def edit
  end

  # POST /rams
  # POST /rams.json
  def create
    @ram = Ram.new(ram_params)

    respond_to do |format|
      if @ram.save
        format.html { redirect_to rams_path, notice: 'Ram was successfully created.' }
        format.json { render action: 'show', status: :created, location: @ram }
      else
        format.html { render action: 'new' }
        format.json { render json: @ram.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rams/1
  # PATCH/PUT /rams/1.json
  def update
    respond_to do |format|
      if @ram.update(ram_params)
        format.html { redirect_to rams_path, notice: 'Ram was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ram.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rams/1
  # DELETE /rams/1.json
  def destroy
    @ram.destroy
    respond_to do |format|
      format.html { redirect_to rams_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.rams << @ram
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add RAM to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
    @computer.rams.remove(@ram)
    render 'index' unless @computer.present?
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't delete RAM from configuration"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ram
      @ram = Ram.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
  def ram_params
    params.require(:ram).permit(HardwareController::PERMITTED_PARAMS + [:capacity, :generation, :frequency])
  end

  def consider_computer
    @rams = Ram.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_ram
    @ram = Ram.new()
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @ram = Ram.find(params[:id]) if params[:id]
      @ram ||= Ram.new(ram_params)
      @ram.parse_from_services
      render :edit
    end
  end

end

