class UsersController < ApplicationController
  before_action :set_user, except: :index

  load_and_authorize_resource

  def index
    @users = @users.page(params[:page])
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path }
      format.json { head :no_content }
    end
  end

  private
  def user_params
    params.require(:user).permit([:username, :email, :password, :password_confirmation, :current_password])
  end

  def set_user
    @user = User.find(params[:id])
  end
end
