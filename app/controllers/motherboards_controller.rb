class MotherboardsController < HardwareController
  # For compatibility with CanCan we must manually set hardware manually to prevent triggering of it's getter
  before_filter :new_motherboard, :only => [:new, :create]

  before_action :reject_empty_params, only: [:create, :update]
  before_action :set_motherboard, except: [:index, :create, :filter, :new]
  before_action :set_computer, except: [:new, :edit, :create, :destroy ]
  append_before_action :consider_computer, only: [:index, :filter]

  load_and_authorize_resource

  [:form_factor, :chipset, :socket, :number_of_sockets, :ram_generations, :number_of_ram_slots, :max_ram_total_size,
   :number_of_sata2_ports, :number_of_pcie_x16_slots, :number_of_pcie_x8_slots, :number_of_pcie_x1_slots, :integrated_audio,
   :supported_multi_gpus, :integrated_gpu].each do |field_symbol|
    scope_symbol = "with_#{field_symbol}".to_sym
    has_scope scope_symbol, type: :array, only: :filter do |controller, scope, values|
      (values.all? &:blank?) ? scope : scope.send(scope_symbol, values)
    end
  end

  has_scope :with_cpu_gpu_support, type: :boolean, only: :filter


  # GET /motherboards
  # GET /motherboards.json
  def index
    @motherboards = apply_scopes(@motherboards).all.page(params[:page])
    @filter_values = @motherboards.filter_values
  end

  def filter
    @filter_values = @motherboards.filter_values
    @motherboards = apply_scopes(@motherboards).all.page(params[:page])
    render :index
  end

  # GET /motherboards/1
  # GET /motherboards/1.json
  def show
  end

  # GET /motherboards/new
  def new
  end

  # GET /motherboards/1/edit
  def edit
  end

  # POST /motherboards
  # POST /motherboards.json
  def create
    @motherboard = Motherboard.new(motherboard_params)

    respond_to do |format|
      if @motherboard.save
        format.html { redirect_to motherboards_path, notice: 'Motherboard was successfully created.' }
        format.json { render action: 'show', status: :created, location: @motherboard }
      else
        format.html { render action: 'new' }
        format.json { render json: @motherboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /motherboards/1
  # PATCH/PUT /motherboards/1.json
  def update
    respond_to do |format|
      if @motherboard.update(motherboard_params)
        format.html { redirect_to motherboards_path, notice: 'Motherboard was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @motherboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /motherboards/1
  # DELETE /motherboards/1.json
  def destroy
    @motherboard.destroy
    respond_to do |format|
      format.html { redirect_to motherboards_url }
      format.json { head :no_content }
    end
  end

  def add_to
    authorize! :add_to, @computer
    @computer.motherboard = @motherboard
    if @computer.save!
      redirect_to edit_computer_url(@computer)
    else
      flash[:alert] = "Can't add motherboard to configuration"
    end
  end

  def remove_from
    authorize! :remove_from, @computer
     @computer.motherboard = nil
     @computer.save
     redirect_to edit_computer_url(@computer)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_motherboard
    @motherboard = Motherboard.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def motherboard_params
    params.require(:motherboard).permit(HardwareController::PERMITTED_PARAMS + [ :form_factor, :socket, :number_of_sockets,
                                        :number_of_sata2_ports, :number_of_sata3_ports, :number_of_usb3_ports,
                                        :number_of_pcie_x16_slots, :number_of_pcie_x1_slots, :number_of_pcie_x8_slots,
                                        :number_of_ram_slots, :max_ram_total_size, :chipset, :front_mic, :front_headphone,
                                        :integrated_gpu, :integrated_audio, supported_multi_gpus: [], ram_generations: []])
  end

  def consider_computer
    @motherboards = Motherboard.computer_compatible(@computer)
  end

  def set_computer
    @computer = Computer.find(params[:computer_id]) if params[:computer_id].present?
  end

  def new_motherboard
    @motherboard = Motherboard.new()
  end

  def reject_empty_params
    params[:motherboard][:supported_multi_gpus].reject!(&:blank?) if params[:motherboard][:supported_multi_gpus].present?
    params[:motherboard][:ram_generations].reject!(&:blank?) if params[:motherboard][:ram_generations].present?
  end

  def parse_from_services
    if params[:commit] == 'Fetch'
      @motherboard = Motherboard.find(params[:id]) if params[:id]
      @motherboard ||= Motherboard.new(motherboard_params)
      @motherboard.parse_from_services
      render :edit
    end
  end

end
