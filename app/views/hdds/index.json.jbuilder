json.array!(@hdds) do |hdd|
  json.extract! hdd,
  json.url hdd_url(hdd, format: :json)
end
