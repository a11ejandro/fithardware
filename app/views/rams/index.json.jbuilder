json.array!(@rams) do |ram|
  json.extract! ram, 
  json.url ram_url(ram, format: :json)
end
