json.array!(@hulls) do |hull|
  json.extract! hull, 
  json.url hull_url(hull, format: :json)
end
