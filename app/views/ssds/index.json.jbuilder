json.array!(@ssds) do |ssd|
  json.extract! ssd, 
  json.url ssd_url(ssd, format: :json)
end
