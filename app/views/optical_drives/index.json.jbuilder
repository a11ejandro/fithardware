json.array!(@optical_drives) do |optical_drive|
  json.extract! optical_drive,
  json.url optical_drive_url(optical_drive, format: :json)
end
