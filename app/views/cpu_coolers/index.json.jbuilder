json.array!(@cpu_coolers) do |cpu_cooler|
  json.extract! cpu_cooler,
  json.url cpu_cooler_url(cpu_cooler, format: :json)
end
