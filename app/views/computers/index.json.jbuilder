json.array!(@computer) do |configuration|
  json.extract! configuration, 
  json.url configuration_url(configuration, format: :json)
end
