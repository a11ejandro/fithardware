json.array!(@video_cards) do |video_card|
  json.extract! video_card,
  json.url video_card_url(video_card, format: :json)
end
