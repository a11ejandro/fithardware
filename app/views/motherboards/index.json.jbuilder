json.array!(@mother_boards) do |mother_board|
  json.extract! mother_board, 
  json.url mother_board_url(mother_board, format: :json)
end
