json.array!(@power_supplies) do |power_supply|
  json.extract! power_supply, 
  json.url power_supply_url(power_supply, format: :json)
end
