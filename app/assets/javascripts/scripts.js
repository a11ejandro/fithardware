/**
 * Created by alex on 24.03.14.
 */
$(document).ready(function() {

    var rangedSliderIDs = []
    $('.range-input').each(function(){ rangedSliderIDs.push(this.id); })

    $.each(rangedSliderIDs, function(index, ID){
        $('#_' + ID + '_min').change(generate_ranged_min_handler(ID));
        $('#_' + ID + '_max').change(generate_ranged_max_handler(ID));

        $('#slider-' + ID).slider({
            range: true,
            slide:  function (event, ui) {
                $("#_" + ID + "_min").val(ui.values[0]);
                $("#_" + ID + "_max").val(ui.values[1]);
            },
            create:  function(event, ui){
                var min_price = parseInt($("#_" + ID + "_min").val());
                var max_price = parseInt($("#_" + ID + "_max").val());
                $(this).slider({
                    range: true,
                    min: parseInt($('#_'+ ID + '_general_min').val()),
                    max: parseInt($('#_'+ ID + '_general_max').val()),
                    values: [min_price, max_price],
                    step: 5
                });
            }
        })
    })


    $('#toggle-guide').click(function() {
        $(this).text(function(i) {
            return $('#guide').is(':visible') ? 'Show Guide' : 'Hide Guide';
        });
        $('#guide').toggle();
    });
});

function generate_ranged_min_handler(ID) {
    return function(event) {
        var min_price = parseInt($("#_" + ID + "_min").val());
        var max_price = parseInt($("#_" + ID + "_max").val());

        if (min_price > max_price) {
            min_price = max_price;
            $('#_' + ID + '_min').val(max_price)
        }
        $('#slider-' +ID).slider('values', [min_price, max_price]);
    }
}


function generate_ranged_max_handler(ID) {
    return function(event) {
        var min_price = parseInt($("#_" + ID + "_min").val());
        var max_price = parseInt($("#_" + ID + "_max").val());

        if (parseInt(min_price) > parseInt(max_price)) {
            max_price = min_price;
            $('#_' + ID + '_max').val(min_price)
        }
        $('#slider-' + ID).slider('values', [min_price, max_price]);
    }
}