class Pack
  include Mongoid::Document

  field :count, type: Integer, default: 1

  belongs_to :pack_set
  belongs_to :hardware

  def type
    return hardware.class.name
  end

end