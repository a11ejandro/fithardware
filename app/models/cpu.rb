class Cpu < Hardware

  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :socket, :architecture, :manufacturing_process, :frequency,
                                :turbo_frequency, :number_of_cores, :number_of_threads, :integrated_gpu, :l2_cache_size,
                                :l3_cache_size, :complectation]
  MANUFACTURERS = %w[Intel AMD]
  COMPLECTATIONS = %w[Box Tray]
  
  field :socket, type: String
  field :integrated_gpu, type: String
  field :frequency, type: Integer
  field :number_of_cores, type: Integer, default: 1
  field :number_of_threads, type: Integer, default: 1
  field :manufacturing_process, type: Integer, default: 32
  field :architecture, type: String
  field :turbo_frequency, type: Integer
  field :l2_cache_size, type: Integer
  field :l3_cache_size, type: Integer
  field :complectation, type: String
  
  validates_presence_of :socket,
                        :frequency,
                        :l2_cache_size,
                        :l3_cache_size,
                        :number_of_cores,
                        :architecture,
                        :manufacturing_process

  validates_inclusion_of :manufacturer, in: MANUFACTURERS
  validates_inclusion_of :complectation, in: COMPLECTATIONS

  scope :intel, where(manufacturer: "Intel")
  scope :amd, where(manufacturer: "AMD")
  scope :with_integrated_gpu_present, where(:integrated_gpu.exists => true)

  [:frequency, :turbo_frequency].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(min, max) { between(field_symbol => min.to_i..max.to_i) }
  end

  [:socket, :integrated_gpu, :architecture, :manufacturing_process, :number_of_cores, :number_of_threads, :l2_cache_size,
   :l3_cache_size, :complectation].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  def self.computer_compatible(computer)
    collection = self
    if computer.present?
      collection = collection.hackintosh_compatible if computer.hackintosh_compatible
      collection = collection.quiet if computer.quiet
      if computer.motherboard.present?
        collection = collection.with_socket(computer.motherboard.socket)
      end

      if computer.cpu_coolers.present?
        cooler_sockets = computer.cpu_coolers.first.sockets

        computer.cpu_coolers.each { |cc| cooler_sockets &= cc.sockets }

        collection = collection.with_socket(cooler_sockets)
      end

      if computer.cpus.present?
        collection = collection.where(_id: computer.cpus.first.object_id)
      end
    end
    return collection
  end

  def self.filter_values
    values = self.standard_filter_values

    self.values_min_max(:frequency, values)
    self.values_min_max(:turbo_frequency, values)

    [:socket, :number_of_cores, :number_of_threads, :l2_cache_size, :l3_cache_size, :integrated_gpu, :architecture,
     :manufacturing_process].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end

    with_gpu_present = self.exists(integrated_gpu: true)
    without_gpu_present = self.exists(integrated_gpu: false)

    values[:integrated_gpu_present] = [true, false] if with_gpu_present && without_gpu_present

    return values
  end

  def fetch_yandex_features(all_features)
    self.socket = all_features['Socket']
    self.number_of_cores = all_features['Количество ядер'].to_i
    self.l2_cache_size = value_from_dimension_string all_features['Объем кэша L2']
    self.l3_cache_size = value_from_dimension_string all_features['Объем кэша L3']
    self.manufacturing_process = value_from_dimension_string all_features['Техпроцесс']
    self.architecture = all_features['Ядро']

    return self
  end

end
