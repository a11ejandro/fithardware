class Hdd < Hardware

  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :capacity, :rpm, :cache_size, :number_of_platters, :sata3_support]

  field :number_of_platters, type: Integer, default: 2
  field :capacity, type: Integer
  field :rpm, type: Integer
  field :cache_size, type: Integer
  field :sata3_support, type: Boolean, default: false
  field :noise_level, type: Integer, default: 35
  
  validates_presence_of :capacity,
                        :rpm,
                        :cache_size,
                        :sata3_support

  [:number_of_platters, :rpm, :cache_size].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  scope :with_capacity, ->(min, max) { between(:capacity => min.to_i..max.to_i) }
  scope :with_sata3_support, where(:sata3_support => true)

  def self.computer_compatible(computer)
    collection = self.all

    if computer.present?
      collection = collection.quiet if computer.quiet
    end
    return collection
  end

  def self.filter_values
    values = self.standard_filter_values

    self.values_min_max(:capacity, values)

    [:manufacturer, :number_of_platters, :rpm, :cache_size, :sata3_support].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? && v != false }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end
    return values
  end

  def fetch_yandex_features(all_features)

    self.capacity = all_features['Объем'].split(' ').first.to_i
    self.cache_size = value_from_dimension_string all_features['Объем буферной памяти']
    self.number_of_platters = all_features['Количество пластин'].to_i
    self.rpm = value_from_dimension_string all_features['Скорость вращения']
    self.sata3_support = (all_features['Подключение'].split(' ')[1] == '6Gb/s')
    self.power_consumption = all_features['Потребляемая мощность'].split(' ').first.to_f
    self.noise_level = value_from_dimension_string all_features['Уровень шума работы']

    return self
  end

end
