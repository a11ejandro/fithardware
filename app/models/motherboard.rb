# encoding: utf-8
class Motherboard < Hardware

  POSSIBLE_FORM_FACTORS = %w(ATX EATX microATX mini-ITX)
  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :form_factor, :chipset, :socket, :number_of_sockets,
                                :number_of_ram_slots, :ram_generations, :max_ram_total_size, :max_ram_frequency, :number_of_sata2_ports,
                                :number_of_sata3_ports, :number_of_pcie_x16_slots, :supported_multi_gpus, :integrated_gpu,
                                :integrated_audio, :number_of_usb3_ports, :number_of_pcie_x1_slots, :number_of_pcie_x8_slots]
  
  field :form_factor, type: String
  field :socket, type: String
  field :number_of_sockets, type: Integer, default: 1
  field :number_of_sata2_ports, type: Integer, default: 0
  field :number_of_sata3_ports, type: Integer, default: 0
  field :number_of_usb_ports, type: Integer, default: 6
  field :number_of_usb3_ports, type: Integer, default: 0
  field :number_of_front_usb2, type: Integer, default: 2
  field :number_of_front_usb3, type: Integer, default: 0
  field :front_mic, type: Mongoid::Boolean, default: false
  field :front_headphone, type: Mongoid::Boolean, default: false
  field :number_of_pcie_x16_slots, type: Integer, default: 1
  field :number_of_pcie_x1_slots, type: Integer, default: 2
  field :number_of_pcie_x8_slots, type: Integer, default: 2
  field :number_of_ram_slots, type: Integer
  field :integrated_audio, type: String
  field :ram_generations, type: Array
  field :max_ram_total_size, type: Float, default: 16
  field :max_ram_frequency, type: Integer, default: 1333
  field :chipset, type: String
  field :supported_multi_gpus, type: Array
  field :integrated_gpu, type: String

  has_many :computers

  validates_presence_of  :socket,
                         :chipset,
                         :number_of_sockets,
                         :number_of_pcie_x16_slots,
                         :number_of_ram_slots,
                         :ram_generations

  validates_inclusion_of :form_factor, in: POSSIBLE_FORM_FACTORS
  validate :ram_generations_validator

  #Scopes for user filtering
  [:form_factor, :chipset, :socket, :number_of_sockets, :ram_generations, :max_ram_total_size, :max_ram_frequency,
   :number_of_ram_slots, :number_of_sata2_ports, :number_of_sata3_ports, :number_of_pcie_x16_slots, :number_of_pcie_x8_slots,
   :number_of_pcie_x1_slots, :integrated_audio, :supported_multi_gpus, :integrated_gpu, :number_of_usb_ports,
   :number_of_usb3_ports, :number_of_front_usb2, :number_of_front_usb3, :front_mic, :front_headphone].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  scope :by_popularity, desc(:computers)

  # Return all possible values to select for each field
  def self.filter_values
    values = self.standard_filter_values

    [:form_factor, :chipset, :socket, :number_of_sockets, :ram_generations, :max_ram_total_size, :number_of_ram_slots,
     :max_ram_frequency, :number_of_sata2_ports, :integrated_audio, :number_of_pcie_x16_slots, :number_of_pcie_x8_slots,
     :number_of_pcie_x1_slots, :supported_multi_gpus, :integrated_gpu, :number_of_usb_ports, :number_of_usb3_ports,
     :number_of_front_usb2, :number_of_front_usb3, :front_mic, :front_headphone].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? && v != false }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end

    multi_gpus_values = self.distinct(:supported_multi_gpus)
    if multi_gpus_values.present?
      multi_gpus_values.reject! { |v| v.is_a?(Array) || v.blank? }
      values[:supported_multi_gpus] = multi_gpus_values.sort_by { |v| v.to_s.downcase } if multi_gpus_values.length > 1
    end
    return values
  end

  def self.computer_compatible(computer)
    collection = self.all
    if computer.present?
      collection = collection.with_form_factor(computer.hull.form_factors) if computer.hull.present?
      collection = collection.hackintosh_compatible if computer.hackintosh_compatible
      if computer.cpus.present?
        collection = collection.with_socket(computer.cpus.first.socket)
        collection = collection.where(:number_of_sockets.gte => computer.cpus.count)
      end
      if computer.rams.present?
        collection = collection.with_ram_generations(computer.rams.first.generation)
        collection = collection.where(:number_of_ram_slots.gte => computer.rams.count)
        collection = collection.where(:max_ram_frequency.gte => computer.rams.first.frequency)
        collection = collection.where(:max_ram_total_size.gte => computer.ram_total_size)
      end

      if computer.cpu_coolers.present?
        cooler_sockets = computer.cpu_coolers.first.sockets

        computer.cpu_coolers.each { |cc| cooler_sockets &= cc.sockets }

        collection = collection.with_socket(cooler_sockets)
      end

      if computer.video_cards.present? and computer.video_cards.count > 1
        collection = collection.where(:number_of_pcie_x16_slots.gte => computer.video_cards.count)
        case computer.video_cards.first.manufacturer
          when 'AMD/ATI'
            collection = collection.with_supported_multi_gpus(['CrossFire'])
          when 'NVIDIA'
            collection = collection.with_supported_multi_gpus(['SLI'])
        end

        unless computer.profi_mode
          collection = collection.where(:number_of_pcie_x16_slots.gte => computer.video_cards.count)
        end
      end
    end
    return collection
  end

  def fetch_yandex_features(all_features)
    self.form_factor = all_features['Форм-фактор']
    self.socket = all_features['Socket'].split('x').length > 1 ? all_features['Socket'].split('x')[1] : all_features['Socket']
    self.number_of_sockets = all_features['Socket'].split('x').length > 1 ? all_features['Socket'].split('x')[0] : 1
    self.chipset = all_features['Чипсет']
    self.number_of_sata2_ports = all_features['SATA'].scan(/количество разъемов SATA 3Gb\/s: (\d+)/).flatten[0].to_i
    self.number_of_sata3_ports = all_features['SATA'].scan(/количество разъемов SATA 6Gb\/s: (\d+)/).flatten[0].to_i
    self.number_of_pcie_x16_slots = all_features['Слоты расширения'].scan(/(\d+)xPCI-E x16/).flatten[0].to_i
    self.number_of_pcie_x1_slots = all_features['Слоты расширения'].scan(/(\d+)xPCI-E x1[,|\z]/).flatten[0].to_i
    self.number_of_pcie_x8_slots = all_features['Слоты расширения'].scan(/(\d+)xPCI-E x8/).flatten[0].to_i
    self.number_of_ram_slots = all_features['Количество слотов памяти'].to_i
    self.ram_generations = all_features['Память'].scan(/(DDR\d)/).flatten
    self.max_ram_total_size = value_from_dimension_string all_features['Максимальный объем памяти']
    self.max_ram_frequency = all_features['Память'].scan(/(\d+) МГц/).flatten[0].to_i
    if all_features['Поддержка SLI/CrossFire']
      self.supported_multi_gpus = all_features['Поддержка SLI/CrossFire'].scan(/(SLI|CrossFire)/).flatten
    end
    if all_features['Встроенный видеоадаптер']
      self.integrated_gpu = all_features['Встроенный видеоадаптер'].scan(/на основе (.*)\z/).flatten[0]
    end

    if all_features['Звук']
      self.integrated_audio = all_features['Звук'].scan(/на основе (.*)\z/).flatten[0]
    end

    self.number_of_usb3_ports = all_features['Наличие интерфейсов'].scan(/USB, из них (\d+) USB 3.0/).flatten[0].to_i
    self.number_of_usb_ports = all_features['Наличие интерфейсов'].scan(/(\d+) USB,/).flatten[0].to_i

    return self
  end

  private

  def ram_generations_validator
    if ram_generations.present?
      self.ram_generations.each do |rg|
        errors.add(:ram_generations, "RAM generation #{rg} is invalid") unless Ram::GENERATIONS.include? rg
      end
    end
  end

end
