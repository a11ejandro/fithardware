# Because of word 'Case' is reserved by Ruby, computers case entity will be named as Hull
class Hull < Hardware

  POWER_SUPPLY_POSITIONS = %w(Vertical Horizontal Outer)

  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :form_factors, :power_supply_power, :power_supply_position,
                                :number_of_storage_slots, :number_of_optical_drives, :number_of_front_usb, :front_usb3,
                                :front_mic, :front_headphone, :front_fire_wire, :power_supply_down, :video_card_max_length,
                                :cpu_cooler_max_height]

  field :number_of_storage_slots, type: Integer, default: 0
  field :number_of_optical_drives, type: Integer, default: 0
  field :video_card_max_length, type: Integer, default: 250
  field :cpu_cooler_max_height, type: Integer, default: 140
  field :form_factors, type: Array
  field :hackintosh_compatible, type: Boolean, default: true
  field :power_supply_power, type: Integer, default: 0
  field :power_supply_position, type: String, default: 'Horizontal'
  field :number_of_front_usb, type: Integer, default: 2
  field :front_usb3, type: Mongoid::Boolean
  field :front_mic, type: Mongoid::Boolean, default: false
  field :front_headphone, type: Mongoid::Boolean, default: false
  field :front_fire_wire, type: Mongoid::Boolean, default: false
  field :power_supply_down, type: Mongoid::Boolean, default: false

  belongs_to :power_supply, dependent: :destroy
  has_many :computers
  
  validates_presence_of :form_factors
  validates_inclusion_of :power_supply_position, in: POWER_SUPPLY_POSITIONS
  validate :form_factor_validator

  [:number_of_storage_slots, :number_of_optical_drives, :form_factors, :power_supply_position, :number_of_front_usb].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  [:front_mic, :front_headphone, :front_fire_wire, :front_usb3].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, where(field_symbol => true)
  end

  scope :with_power_supply_power, ->(min, max) {between(:power_supply_power => min..max)}


  before_save do |document|
    if document.power_supply_power > 0 && document.power_supply.nil?
      document.power_supply = PowerSupply.create({manufacturer: document.manufacturer, product_name: "Hull's #{document.product_name} Power Supply",
                                                  image: document.image, power: document.power_supply_power, number_of_6_pin: 1,
                                                  number_of_6_2_pin: 1, number_of_sata_connectors: 3})
    end
  end

  def self.computer_compatible(computer)
    collection = self
      if computer.present?
        without_supplies = collection.exists(:power_supply => false)
        if computer.power_supply.present?
          collection = without_supplies
        else
          collection_supplies = collection.pluck(:power_supply)
          compatible_supplies = PowerSupply.in(id: collection_supplies).computer_compatible(computer)

          collection = collection.or({:power_supply.in => compatible_supplies.to_a.map(&:id)}, {:id.in => without_supplies.to_a.map(&:id)})
        end

        collection = collection.any_in(:form_factors => computer.motherboard.form_factor) if computer.motherboard.present?

        if computer.cpu_coolers.present?
          max_height = computer.cpu_coolers.to_a.map{|c| c.unpacked_height}.max
          collection = collection.gt(:cpu_cooler_max_height => max_height)
        end

        if computer.video_cards.present?
          max_vc_length = computer.video_cards.to_a.map{|vc| vc.unpacked_length}.max
          max_vc_width = computer.video_cards.to_a.map{|vc| vc.unpacked_width}.max

          collection = collection.and({:video_card_max_length.gt => max_vc_length },
          {:unpacked_width.gt => (max_vc_width * Computer::MARGIN_OF_SIZE_FITTING)})
        end

        collection = collection.gte(:number_of_storage_slots => computer.necessary_sata_ports)
        collection = collection.gte(:number_of_optical_drives => computer.optical_drives.count)
        collection = collection.where(:hackintosh_compatible => true) if computer.hackintosh_compatible
      end
    return collection
  end

  def self.filter_values
    values = self.standard_filter_values
    self.values_min_max(:power_supply_power, values)

    [:number_of_storage_slots, :number_of_optical_drives, :form_factors, :power_supply_position, :front_usb3, :front_mic,
     :front_headphone, :front_fire_wire].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end
    return values
  end

  def fetch_yandex_features(all_features)
    self.power_supply_power = value_from_dimension_string all_features['Блок питания']
    self.power_supply_down = all_features['Блок питания внизу'].present?
    self.form_factors = all_features['Форм-фактор'].gsub(/mATX|Mini-ITX/, 'mATX'=>'microATX', 'Mini-ITX'=>'mini-ITX').split(', ')

    front_panel = all_features['Разъемы на лицевой панели']
    self.front_headphone = front_panel.include? 'наушники'
    self.front_mic = front_panel.include? 'микрофон'
    self.number_of_front_usb = front_panel.split('USB x')[1].first.to_i
    self.front_usb3 = front_panel.include? 'USB 3.0'
    self.front_fire_wire = front_panel.include? 'FireWire'

    case all_features['Расположение блока питания']
      when 'горизонтальное'
        self.power_supply_position = 'Horizontal'

      when 'вертикальное'
        self.power_supply_position = 'Vertical'

      else
        self.power_supply_position = 'Outer'
    end

    self.number_of_storage_slots = all_features['Число внутренних отсеков 3,5"'].to_i
    self.number_of_optical_drives = all_features['Число отсеков 5,25"'].to_i
    self.cpu_cooler_max_height = value_from_dimension_string all_features['Максимальная высота процессорного кулера'] || self.cpu_cooler_max_height
    self.video_card_max_length = value_from_dimension_string all_features['Максимальная длина видеокарты'] || self.video_card_max_length

    return self
  end



  private

  def form_factor_validator
    form_factors.each do |form_factor|
      errors.add(:form_factors, "Format doesn't exist") unless Motherboard::POSSIBLE_FORM_FACTORS.include? form_factor
    end
  end
end
