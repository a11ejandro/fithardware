class Ram < Hardware

  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :generation, :capacity, :frequency]
  GENERATIONS = %w(DDR2 DDR3 DDR4)

  field :generation, type: String
  field :capacity, type: Float
  field :frequency, type: Integer
  field :hackintosh_recommended, type: Boolean, default: true
  field :power_consumption, type: Float, default: 3.0

  validates_presence_of :generation,
                        :capacity,
                        :frequency

  validates_inclusion_of :generation, in: GENERATIONS

  [:generation, :capacity, :frequency].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  def self.filter_values
    values = self.standard_filter_values

    [:generation, :capacity, :frequency].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end
    return values
  end

  def self.computer_compatible(computer)
    collection = self
    if computer.present?
      if computer.motherboard.present?
        collection = collection.with_generation(computer.motherboard.ram_generations)
        collection = collection.where(:capacity.lte => computer.free_ram_size)
        collection = collection.where(:frequency.lte => computer.motherboard.max_ram_frequency)
      end

      if computer.rams.present?
        collection = collection.with_generation(computer.rams.first.generation)
        collection = collection.where(:frequency => computer.rams.first.frequency)
      end
    end
    return collection
  end

  def fetch_yandex_features(all_features)
    self.generation = all_features['Тип памяти']
    self.frequency = value_from_dimension_string all_features['Тактовая частота']
    self.items_in_pack = all_features['Объем'].scan(/\d+/).first
    self.capacity = all_features['Объем'].scan(/\d+/)[1]
  end
end
