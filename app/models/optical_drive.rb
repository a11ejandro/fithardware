class OpticalDrive < Hardware

  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :cd_read, :cd_r_write, :cd_rw_write, :dvd_read, :dvd_r_write,
  :dvd_rw_write, :dvd_dl_write, :bd_read, :bd_r_write, :bd_rw]

  field :cd_r_write, type: Integer, default: 0
  field :cd_rw_write, type: Integer, default: 0
  field :dvd_r_write, type: Integer, default: 0
  field :dvd_rw_write, type: Integer, default: 0
  field :dvd_dl_write, type: Integer, default: 0
  field :bd_r_write, type: Integer, default: 0
  field :bd_re_write, type: Integer, default: 0
  field :bd_dl_write, type: Integer, default: 0
  field :dvd_read, type: Integer, default: 0
  field :bd_read, type: Integer, default: 0
  field :cd_read, type: Integer, default: 0

  validates_presence_of :cd_read

  [:cd_r_write, :cd_rw_write, :dvd_read, :dvd_r_write, :dvd_rw_write, :dvd_dl_write, :bd_read,
   :bd_r_write, :bd_re_write, :bd_dl_write].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(min, max) {between(field_symbol => min.to_i..max.to_i)}
  end


  def self.computer_compatible(computer)
    collection = self.all
    if computer.present?
      collection = collection.quiet if computer.quiet
    end
    return collection
  end

  def self.filter_values
    values = self.standard_filter_values
    [:cd_r_write, :cd_rw_write, :dvd_read, :dvd_r_write, :dvd_rw_write, :dvd_dl_write, :bd_read,
     :bd_r_write, :bd_re_write, :bd_dl_write].each do |field_symbol|
      self.values_min_max(field_symbol, values)
    end

    return values
  end

  def fetch_yandex_features(all_features)
    self.cd_read = speed_string_to_int all_features['Максимальная скорость чтения CD']
    self.cd_r_write = speed_string_to_int all_features['Максимальная скорость записи CD-R']
    self.cd_rw_write = speed_string_to_int all_features['Максимальная скорость записи CD-RW']
    self.dvd_read = speed_string_to_int all_features['Максимальная скорость чтения DVD']
    self.dvd_r_write = speed_string_to_int all_features['Максимальная скорость записи DVD-R']
    self.dvd_rw_write = speed_string_to_int all_features['Максимальная скорость записи DVD-RW']
    self.dvd_dl_write = speed_string_to_int all_features['Максимальная скорость записи DVD+R DL']
    self.bd_read = speed_string_to_int all_features['Максимальная скорость чтения BD-ROM']
    self.bd_r_write = speed_string_to_int all_features['Максимальная скорость записи BD-R']
    self.bd_re_write = speed_string_to_int all_features['Максимальная скорость записи BD-RE']
    self.bd_dl_write = speed_string_to_int all_features['Максимальная скорость записи BD-RE DL']

    self.power_consumption ||= all_features['Потребляемая мощность'].split(' ').first.to_f if all_features['Потребляемая мощность'].present?

    return self
  end

  private

  def speed_string_to_int(speed_string)
    result = nil
    result = speed_string.chomp('x').to_i if speed_string.present?
    return result
  end

end