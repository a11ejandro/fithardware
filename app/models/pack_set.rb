class PackSet
  include Mongoid::Document
  include Enumerable

  field :count, type: Integer, default: 0
  field :type, type: String

  has_many   :packs, autosave: true, dependent: :destroy

  belongs_to :rams, class_name: 'Computer', inverse_of: :rams
  belongs_to :cpus, class_name: 'Computer', inverse_of: :cpus
  belongs_to :ssds, class_name: 'Computer', inverse_of: :ssds
  belongs_to :hdds, class_name: 'Computer', inverse_of: :hdds
  belongs_to :optical_drives, class_name: 'Computer', inverse_of: :optical_drives
  belongs_to :video_cards, class_name: 'Computer', inverse_of: :video_cards
  belongs_to :cpu_coolers, class_name: 'Computer', inverse_of: :cpu_coolers


  validate :type_validator

  before_save :calculate_properties
  after_save  { self.role.unvalidate if self.role.present? && !self.role.new? }

  def self.create_with_hardware(hardware, count = 1)
    self.create(packs: [Pack.new(hardware: hardware, count: count)])
  end

  def each
    if packs.present?
      packs.each do |pack|
        pack.count.times { yield pack.hardware }
      end
    end
  end

  def << (hardware)
    added_flag = false
    if packs.present?
      packs.each do |pack|
        next if added_flag
        if pack.hardware == hardware
          pack.count += hardware.items_in_pack
          pack.save
          added_flag = true
        end
      end
    end
    unless added_flag
      packs << Pack.create(hardware: hardware, count: hardware.items_in_pack)
    end
    self.save
  end

  def remove(hardware)
    packs.each do |pack|
      if pack.hardware == hardware
        if pack.count <= hardware.items_in_pack
          pack.delete
        else
          pack.count -= hardware.items_in_pack
          pack.save
        end
      end
    end
    self.save
  end

  def include?(hardware)
    return false unless packs.present?

    packs.each do |pack|
      if pack.hardware == hardware
        return true
      end
    end
    return false
  end

  def present?
    return packs.present?
  end

  def empty?
    return packs.empty?
  end

  def calculate_properties
    counter = 0
    self.packs.each {|p| counter += p.count}
    self.count = counter

    self.type = packs.first.type if packs.present?
  end

  def role
    [cpus, cpu_coolers, rams, hdds, ssds, optical_drives, video_cards].compact.first
  end

  private

  def type_validator
    if packs.present?
      errors.add(:packs, "Must present the same type of hardware") unless packs.all?{|p| p.type == packs.first.type}
    end
  end
end