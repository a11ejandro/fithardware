class Computer
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Attributes

  MARGIN_OF_POWER_SUPPLY_SAFETY = 1.2
  MARGIN_OF_SIZE_FITTING = 0.9

  field :violations, type: Array, default: []
  field :state, default: 'new'
  field :name, type: String
  field :description, type: String, default: 'Description is empty'
  field :power_consumption, type: Integer, default: 0
  field :hackintosh_compatible, type: Mongoid::Boolean, default: false
  field :quiet, type: Mongoid::Boolean, default: false
  field :profi_mode, type: Mongoid::Boolean, default: false
  field :amazon_price, type: Float, default: 0.00
  field :hardware_counter, type: Integer, default: 1
  field :weight, type: Float, default: 0

  belongs_to              :hull
  belongs_to              :motherboard
  belongs_to              :power_supply
  has_one                 :cpus, class_name: 'PackSet', inverse_of: :cpus
  has_one                 :cpu_coolers, class_name: 'PackSet', inverse_of: :cpu_coolers
  has_one                 :rams, class_name: 'PackSet', inverse_of: :rams
  has_one                 :ssds, class_name: 'PackSet', inverse_of: :ssds
  has_one                 :hdds, class_name: 'PackSet', inverse_of: :hdds
  has_one                 :video_cards, class_name: 'PackSet', inverse_of: :video_cards
  has_one                 :optical_drives, class_name: 'PackSet', inverse_of: :optical_drives

  belongs_to :user

  scope :published, ->() { where(:state => 'published') }

  state_machine :state, initial: :new do

    event :publish do
      transition :new => :published
      transition :published => same
    end

    event :unvalidate do
      transition any => :new
    end

    state :published do
      validates_presence_of :rams,
                            :hull,
                            :cpus,
                            :motherboard,
                            :power_supply,
                            :power_consumption

      validate              :cpus_validator
      validate              :form_factor_validator
      validate              :storage_validator
      validate              :rams_validator
      validate              :power_validator
      validate              :videocards_validator
      validate              :video_validator
      validate              :sizes_validator
      validate              :cooling_validator
    end
  end

  before_save :update_counters
  after_validation :check_state

  set_callback(:create, :before) do |document|

    document.create_rams
    document.create_cpus
    document.create_ssds
    document.create_hdds
    document.create_video_cards
    document.create_optical_drives
    document.create_cpu_coolers
  end

  def free_sockets
    if motherboard.present?
      cpus.present? ? (motherboard.number_of_sockets - cpus.count) : motherboard.number_of_sockets
    else
      1
    end
  end

  def free_cpu_cooler_places
    if motherboard.present?
      cpu_coolers.present? ? (motherboard.number_of_sockets - cpu_coolers.count) : motherboard.number_of_sockets
    else
      1
    end
  end

  def free_ram_slots
    if motherboard.present?
      rams.present? ? (motherboard.number_of_ram_slots - rams.count) : motherboard.number_of_ram_slots
    else
      1
    end
  end

  def ram_total_size
    rams.reduce(0) { |val, ram| val + ram.capacity}
  end

  def free_ram_size
    free_size = 10000000
    if motherboard.present?
      free_size = motherboard.max_ram_total_size
      free_size -= ram_total_size
    end
    return free_size
  end

  def free_drive_slots
    if motherboard.present?
      if hull.present?
        number_of_free_slots =  [(motherboard.number_of_sata2_ports + motherboard.number_of_sata3_ports),
                                 hull.number_of_storage_slots].min
      else
        number_of_free_slots = motherboard.number_of_sata2_ports + motherboard.number_of_sata3_ports
      end
    elsif hull.present?
      number_of_free_slots = hull.number_of_storage_slots
    else
      return 1
    end

    return number_of_free_slots - necessary_sata_ports.to_i
  end

  def free_optical_drive_places
    places_in_hull = hull.present? ? (hull.number_of_optical_drives - optical_drives.count) : 1
    return [places_in_hull, free_drive_slots].min
  end

  def necessary_sata_ports
    [ssds, hdds, optical_drives].reduce(0) { |val, collection| val + collection.count}
  end

  def free_video_card_slots
    if motherboard.present?
      video_cards.present? ? (motherboard.number_of_pcie_x16_slots - video_cards.count) : (motherboard.number_of_pcie_x16_slots)
    else
      1
    end
  end

  def calculate_power_consumption
    self.power_consumption = motherboard.present? ? motherboard.power_consumption : 0
    
    [cpus, ssds, hdds, optical_drives, rams, video_cards].each do |hardware_set|
      if hardware_set.present?
        hardware_set.each { |consumer| self.power_consumption += consumer.power_consumption }
      end
    end

    self.power_consumption *= MARGIN_OF_POWER_SUPPLY_SAFETY
  end

  def necessary_6_pin
    video_cards.to_a.reduce(0) {|total, vc| total + vc.number_of_6_pin }
  end

  def necessary_8_pin
    video_cards.to_a.reduce(0) {|total, vc| total + vc.number_of_8_pin }
  end


  private

  def sizes_validator
    if hull.present?
      if video_cards.present?
        video_cards.each do |vc|
          errors.add(:video_cards, 'Video card is wider than case') if vc.unpacked_width * MARGIN_OF_SIZE_FITTING >= hull.unpacked_width
          errors.add(:video_cards, 'Video card is longer than case') if vc.unpacked_length >= hull.video_card_max_length
        end
      end

      fitting_cpu_coolers = 0
      uncooled_cpus =  0

      if cpus.present?
        cpus.each do |cpu|
          uncooled_cpus += 1 if ((cpu.unpacked_height > hull.cpu_cooler_max_height) || (cpu.complectation == 'Tray'))
        end
      end

      if cpu_coolers.present?
        cpu_coolers.each { |cc| fitting_cpu_coolers += 1 if cc.unpacked_height < hull.cpu_cooler_max_height * MARGIN_OF_SIZE_FITTING }
      end

      errors.add(:cpu_coolers, 'Not enough cpu coolers of fitting size') if fitting_cpu_coolers < uncooled_cpus
    end
  end

  def cooling_validator
    uncooled_cpus = 0

    if cpus.present?
      cpus.each { |cpu| uncooled_cpus += 1 if cpu.complectation == 'Tray' }
    end

    errors.add(:cpu_coolers, 'Computer has a cpu without cooling system') if cpu_coolers.count < uncooled_cpus

    if cpu_coolers.present? && cpus.present?
      cpu_coolers.each do |cpu_cooler|
        unless cpu_cooler.sockets.include? cpus.first.socket
          errors.add(:cpu_coolers, "CPU cooler #{cpu_cooler.product_name} doesn't support socket #{cpus.first.socket}")
        end
      end
    end
  end

  def presence_validator
    return true if profi_mode
    rams.any? && hull.present? && cpu.present? && motherboard.present? && power_supply.present?
  end

  def storage_validator
    unless profi_mode
      errors.add(:hdds, "Must have at least one storage") if ssds.empty? && hdds.empty?
      if motherboard
        if (motherboard.number_of_sata2_ports + motherboard.number_of_sata3_ports) < necessary_sata_ports
          errors.add(:motherboard, "Not enough SATA ports")
        end
      end
    end
  end

  def rams_validator
    if motherboard && rams.present?
      rams.each do |ram|
        errors.add(:rams, "Motherboard doesn't support this generation of RAM") unless motherboard.ram_generations.include? ram.generation
        errors.add(:rams, "All RAMs must have the same generation") unless ram.generation == rams.first.generation
        errors.add(:rams, "All RAMs must have the same frequency") unless ram.frequency == rams.first.frequency
      end
      errors.add(:rams, "Motherboard doesn't have so many RAM slots") if motherboard.number_of_ram_slots < rams.count
      errors.add(:rams, "Motherboard doesn't support so big ram capacity") if free_ram_size < 0
      errors.add(:rams, "RAM can not be run with standart frequencies") if motherboard.max_ram_frequency < rams.first.frequency
    end
  end

  def cpus_validator
    if motherboard && cpus.present?
      errors.add(:cpus, "Not enough cpu sockets") if cpus.count > motherboard.number_of_sockets
      cpus.each do |cpu|
        errors.add(:cpu, "Processor socket type mismatch") unless cpu.socket == motherboard.socket
      end
    end
  end

  def form_factor_validator
    if hull && motherboard
      errors.add(:hull, "Motherboard doesn't fit case") unless hull.form_factors.to_a.include? motherboard.form_factor
    end
  end

  def power_validator
    if power_supply

      errors.add(:power_supply, "Not enough power for system. Select more powerful power supply") if power_consumption > power_supply.power


      required_universal_connectors = (necessary_6_pin - power_supply.number_of_6_pin) + (necessary_8_pin - power_supply.number_of_8_pin)
      errors.add(:power_supply, "Not enough pin connectors") if (power_supply.number_of_6_2_pin < required_universal_connectors)
    end
  end

  def videocards_validator
    if video_cards.present? && motherboard
      cards = video_cards.to_a
      errors.add(:motherboard, "Motherboard doesn't have enough video card slots") if cards.count > motherboard.number_of_pcie_x16_slots

      if cards.count > 1 && !profi_mode
        if motherboard.supported_multi_gpus.blank?
          errors.add(:motherboard, 'Motherboard doesn\'t support video cards aggregation')
        else
          tmp = video_cards.first.multi_gpu_technology
          cards.each do |video_card|
            errors.add(:video_cards, "Uncompatible multi-GPU support between motherboard and videocard") unless motherboard.supported_multi_gpus.include? video_card.multi_gpu_technology
            errors.add(:video_cards, "Uncompatible multi-GPU support between videocards") unless video_card.multi_gpu_technology == tmp
          end
        end
      elsif cards.count > 1 && profi_mode
        errors.add(:video_cards, 'Number of video cards is greater than number of pci-e slots') if videocards.count > (motherboard.number_of_videocards + motherboard.number_of_pcie_x1_slots + motherboard.number_of_pcie_x8_slots)
      end
    end
  end

  def video_validator
    if motherboard
      unless video_cards.present? || motherboard.integrated_gpu.present? || cpu.integrated_video.present?
        errors.add(:video_cards, 'No graphical system present')
      end
    end
  end

  def update_counters
    temp_price = 0
    temp_counter = 0
    temp_weight = 0
    [hull, motherboard, power_supply, cpus, rams, ssds, hdds, optical_drives, video_cards].each do |hardware|
      if hardware.present?
        if hardware.is_a? PackSet
          hardware.each do |h|
            temp_price += h.amazon_price
            temp_weight += h.unpacked_weight.to_f
          end
          temp_counter += hardware.count
        else
          temp_price += hardware.amazon_price
          temp_counter += 1
          temp_weight = hardware.unpacked_weight.to_f
        end
      end
    end

    self.amazon_price = temp_price
    self.hardware_counter = temp_counter
    self.weight = temp_weight

    calculate_power_consumption
  end

  def check_state
    if self.errors.any?
      self.violations = self.errors.full_messages
      self.unvalidate
    elsif self.published?
      self.violations = []
    end
    return true
  end

end
