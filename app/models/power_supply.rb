class PowerSupply < Hardware

  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :power, :number_of_6_pin, :number_of_8_pin, :number_of_6_2_pin,
                                :certificate_80_plus, :modular_cables]
  CERTIFICATES = %w(Bronze Gold Titanium Platinum Silver none)

  field :noise_level, type: Integer, default: 30
  field :power, type: Integer
  field :number_of_6_pin, type: Integer, default: 0
  field :number_of_8_pin, type: Integer, default: 0
  field :number_of_6_2_pin, type: Integer, default: 0
  field :number_of_sata_connectors, type: Integer
  field :hackintosh_recommended, type: Mongoid::Boolean, default: true
  field :modular_cables, type: Mongoid::Boolean, default: false
  field :certificate_80_plus, type: String, default: 'none'

  has_one :hull
  has_many :computers

  validates_presence_of :power,
                        :number_of_sata_connectors
  validates_inclusion_of :certificate_80_plus, in: CERTIFICATES


  scope :with_power, ->(min, max) { between(:power => min.to_i..max.to_i) }

  [:number_of_6_pin, :number_of_8_pin, :number_of_6_2_pin, :certificate_80_plus, :number_of_sata_connectors].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  scope :with_modular_cables, where(:modular_cables => true)

  def self.computer_compatible(computer)
    collection = self.all
    if computer.present?
      collection = collection.where(:power.gte => computer.power_consumption)
      collection = collection.quiet if computer.quiet?

      collection = collection.where(:number_of_sata_connectors.gte => computer.necessary_sata_ports)

      if computer.video_cards.present?
        necessary_6_pin = computer.necessary_6_pin
        necessary_8_pin = computer.necessary_8_pin
        collection = collection.where("this.number_of_6_2_pin >= ((#{necessary_6_pin} - this.number_of_6_pin) + (#{necessary_8_pin} - this.number_of_8_pin))")
      end
    end
    return collection
  end

  def self.filter_values
    values = self.standard_filter_values

    self.values_min_max(:power, values)

    [:number_of_6_pin, :number_of_8_pin, :number_of_6_2_pin, :number_of_sata_connectors, :certificate_80_plus].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? && v != false }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end

    return values
  end


  def fetch_yandex_features(all_features)

    self.power = value_from_dimension_string all_features['Мощность']
    self.number_of_6_pin = count_pins(all_features['Количество разъемов 6-pin PCI-E'])
    self.number_of_8_pin = count_pins(all_features['Количество разъемов 8-pin PCI-E'])
    self.number_of_6_2_pin = count_pins(all_features['Количество разъемов 6+2-pin PCI-E'])
    self.number_of_sata_connectors = count_pins(all_features['Количество разъемов 15-pin SATA'])
    self.certificate_80_plus = (all_features['Сертификат 80 PLUS'].empty? || all_features['Сертификат 80 PLUS'] == 'простой') ?
        'none' : all_features['Сертификат 80 PLUS']
    self.modular_cables = all_features['Отстегивающиеся кабели'].present?
    self.noise_level = value_from_dimension_string all_features['Уровень шума']
    self.noise_level = 0 if all_features['Система охлаждения'] == 'безвинтеляторный'

    return self
  end

  private

  def count_pins(pin_string)
    result = nil
    result = pin_string.to_i if pin_string.present?
    return result
  end

end
