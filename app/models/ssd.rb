class Ssd < Hardware

  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :capacity, :sata3_support, :read_speed, :write_speed, :random_write_speed]

  field :capacity, type: Integer
  field :sata3_support, type: Boolean, default: false
  field :read_speed, type: Integer
  field :write_speed, type: Integer
  field :random_write_speed, type: Integer
  
  validates_presence_of :capacity, :sata3_support, :random_write_speed

  scope :with_sata3_support, where(:sata3_support => true)

  [:capacity, :read_speed, :write_speed, :random_write_speed].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(min, max) {between(field_symbol => min.to_i..max.to_i)}
  end

  def self.computer_compatible(computer)
    return self.all
  end

  def self.filter_values
    values = self.standard_filter_values

    [:capacity, :read_speed, :write_speed, :random_write_speed].each do |field_symbol|
      self.values_min_max(field_symbol, values)
    end

    temp_values = self.distinct(:sata3_support)
    temp_values.reject! { |v| v.blank? && v != false }
    # If there is only one option, the choice is an illusion
    if temp_values.present? && temp_values.length > 1
        values[:sata3_support] = temp_values.sort_by! { |e| e.to_s.downcase }
    end

    return values
  end

  def fetch_yandex_features(all_features)

    self.capacity = all_features['Объем'].split(' ').first.to_i
    self.read_speed = all_features['Скорость записи/Скорость чтения'].split('/').first.to_i
    self.write_speed = value_from_dimension_string all_features['Скорость записи/Скорость чтения'].split('/')[1]
    self.random_write_speed = value_from_dimension_string all_features['Скорость случайной записи (блоки по 4Кб)']
    self.sata3_support = (all_features['Подключение'].split(' ')[1] == '6Gb/s')
    self.power_consumption = all_features['Потребляемая мощность'].split(' ').first.to_f

    return self
  end

end
