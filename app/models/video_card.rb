class VideoCard < Hardware

  CHIPSET_MANUFACTURERS = %w(NVIDIA AMD/ATI)
  MULTI_GPU_TECHNOLOGIES = %w(SLI CrossFire)
  MEMORY_TYPES = %w(GDDR3 GDDR4 GDDR5)
  ORDERED_FIELDS_FOR_FILTERS = [:amazon_price, :manufacturer, :chipset, :chipset_manufacturer, :chipset_frequency, :memory_size,
  :memory_frequency, :memory_type, :number_of_universal_processors, :system_bus_bit, :number_of_supported_monitors, :max_resolution,
  :multi_gpu_technology, :number_of_6_pin, :number_of_8_pin]

  field :chipset, type: String
  field :chipset_manufacturer, type: String
  field :chipset_frequency, type: Integer
  field :memory_size, type: Integer
  field :memory_frequency, type: Integer
  field :memory_type, type: String
  field :number_of_6_pin, type: Integer, default: 1
  field :number_of_8_pin, type: Integer, default: 0
  field :number_of_supported_monitors, type: Integer, default: 1
  field :number_of_universal_processors, type: Integer
  field :max_resolution, type: String
  field :multi_gpu_technology, type: String
  field :system_bus_bit, type: Integer, default: 64
  
  validates_presence_of :memory_size, :performance_score, :chipset, :chipset_manufacturer, :chipset_frequency, :memory_type,
                        :memory_frequency, :system_bus_bit, :number_of_universal_processors
  validates_inclusion_of :chipset_manufacturer, in: CHIPSET_MANUFACTURERS
  validates_inclusion_of :multi_gpu_technology, in: MULTI_GPU_TECHNOLOGIES, allow_nil: true
  validates_inclusion_of :memory_type,          in: MEMORY_TYPES

  [:chipset, :chipset_manufacturer, :memory_type, :number_of_6_pin, :number_of_8_pin, :system_bus_bit, :number_of_supported_monitors,
   :max_resolution, :multi_gpu_technology].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  [:chipset_frequency, :memory_size, :memory_frequency, :number_of_universal_processors].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(min, max) {between(field_symbol => min.to_i..max.to_i)}
  end

  def self.computer_compatible(computer)
    collection = self
      if computer.present?
        collection = collection.hackintosh_compatible if computer.hackintosh_compatible
        collection = collection.quiet if computer.quiet
        if computer.video_cards.present?
          sample_vc = computer.video_cards.first
          if sample_vc.multi_gpu_technology
            collection = collection.and({:chipset_manufacturer => sample_vc.chipset_manufacturer},
              {:multi_gpu_technology => sample_vc.multi_gpu_technology})
          end
        end
        if computer.hull.present?
          collection = collection.and({:unpacked_width.lt => computer.hull.unpacked_width/Computer::MARGIN_OF_SIZE_FITTING},
            {:unpacked_length.lt => computer.hull.video_card_max_length})
        end
      end

    return collection
  end

  def self.filter_values
    values = self.standard_filter_values
    [:chipset_frequency, :memory_frequency, :memory_size, :number_of_universal_processors].each do |field_symbol|
      self.values_min_max(field_symbol, values)
    end

    [:chipset, :chipset_manufacturer, :memory_type, :number_of_6_pin, :number_of_8_pin, :system_bus_bit,
     :number_of_supported_monitors, :max_resolution, :multi_gpu_technology].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end

    return values
  end

  def fetch_yandex_features(all_features)

    self.chipset = all_features['Графический процессор'].sub(/[0-9] x /, '')
    self.chipset_manufacturer = self.chipset.split(' ').first == 'NVIDIA' ? 'NVIDIA' : 'AMD/ATI'
    self.chipset_frequency = value_from_dimension_string all_features['Частота графического процессора']
    self.number_of_supported_monitors = all_features['Количество поддерживаемых мониторов'].to_i
    self.memory_size = value_from_dimension_string all_features['Объем видеопамяти']
    self.memory_type = all_features['Тип видеопамяти']
    self.memory_frequency = value_from_dimension_string all_features['Частота видеопамяти']
    self.system_bus_bit = value_from_dimension_string all_features['Разрядность шины видеопамяти']
    self.max_resolution = all_features['Максимальное разрешение']
    self.noise_level = 0 if all_features['Охлаждение'] == 'пассивное'
    self.number_of_universal_processors = all_features['Число универсальных процессоров'].to_i

    if all_features['Поддержка режима SLI/CrossFire'].present?
      self.multi_gpu_technology = self.chipset_manufacturer == 'NVIDIA' ? 'SLI' : 'CrossFire'
    end

    necessary_extra_power = all_features['Необходимость дополнительного питания']
    unless necessary_extra_power.empty?
      self.number_of_6_pin = necessary_extra_power.scan(/6/).count
      self.number_of_8_pin = necessary_extra_power.scan(/8/).count
    end

    return self
  end
end
