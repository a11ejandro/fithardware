class CpuCooler < Hardware

  FORM_FACTORS = %w(Tower TOP Liquid)

  field :sockets, type: Array
  field :form_factor, type: String, default: 'Tower'
  field :min_rotation_speed, type: Integer
  field :max_rotation_speed, type: Integer

  validates_presence_of :sockets
  validates_inclusion_of :form_factor, in: FORM_FACTORS

  [:sockets, :form_factor].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(arguments) { any_in(field_symbol => arguments) }
  end

  [:min_rotation_speed, :max_rotation_speed].each do |field_symbol|
    scope "with_#{field_symbol}".to_sym, ->(min, max) {between(field_symbol => min.to_i..max.to_i)}
  end

  def self.computer_compatible(computer)
    collection = self

    if computer.present?
      collection = collection.quiet if computer.quiet
      collection = collection.with_sockets(computer.cpus.first.socket) if computer.cpus.present?
      collection = collection.where(:unpacked_height.lt => computer.hull.cpu_cooler_max_height) if computer.hull.present?
    end

    return collection
  end

  def self.filter_values
    values = self.standard_filter_values

    self.values_min_max(:min_rotation_speed, values)
    self.values_min_max(:max_rotation_speed, values)

    [:sockets, :form_factor, :unpacked_height, :unpacked_width, :unpacked_length].each do |field_symbol|
      temp_values = self.distinct(field_symbol)
      temp_values.reject! { |v| v.blank? && v != false }
      # If there is only one option, the choice is an illusion
      if temp_values.present? && temp_values.length > 1
        values[field_symbol] = temp_values.sort_by! { |e| e.to_s.downcase }
      end
    end

    return values
  end

  def fetch_yandex_features(all_features)
    self.sockets = all_features['Socket'].split(/, |\//) if all_features['Socket'].present?
    self.noise_level = value_from_dimension_string(all_features['Уровень шума'].split(' - ')[1]) if all_features['Уровень шума'].present?
    self.form_factor = 'Liquid' if all_features['Водяное охлаждение'].present?
    self.min_rotation_speed = all_features['Скорость вращения'].split(' - ').first.to_i if all_features['Скорость вращения'].present?
    self.max_rotation_speed = value_from_dimension_string(all_features['Скорость вращения'].split(' - ')[1]) if all_features['Скорость вращения'].present?

    return self
  end

end
