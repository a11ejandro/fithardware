class Hardware
  include Mongoid::Document
  include Mongoid::Timestamps

  require 'amazon_api'
  require 'conversion_coefficients'
  require 'open-uri'
  require 'nokogiri'

  FILTERABLE_FIELDS = %i(hackintosh_recommended, manufacturer, noise_level, performance_score)

  field :image
  field :remote_image_url
  field :amazon_link, type: String
  field :amazon_asin, type: String
  field :amazon_price, type: Float, default: 0

  field :packed_height, type: Float, default: 0
  field :packed_width, type: Float, default: 0
  field :packed_length, type: Float, default: 0
  field :packed_weight, type: Float, default: 0

  field :unpacked_height, type: Float, default: 0
  field :unpacked_width, type: Float, default: 0
  field :unpacked_length, type: Float, default: 0
  field :unpacked_weight, type: Float, default: 0

  field :yandex_market_link, type: String
  field :manufacturer, type: String
  field :product_name, type: String
  field :description, type: String, localize: true
  field :hackintosh_recommended, type: Mongoid::Boolean, default: false
  field :power_consumption, type: Float, default: 0
  field :performance_score, type: Integer, default: 0
  field :noise_level, type: Float, default: 0

  field :items_in_pack, type: Integer, default: 1
  has_many :packs

  mount_uploader :image, HardwareImageUploader

  validates_presence_of :manufacturer
  validates :product_name, presence: true, uniqueness: { :scope => :manufacturer }
  validates_numericality_of :power_consumption
  validates_uniqueness_of :amazon_asin, allow_nil: true

  scope :hackintosh_compatible, where(:hackintosh_recommended => true)
  scope :with_manufacturer, ->(manufacturers) { any_in(:manufacturer => manufacturers) }
  scope :quiet, where(:noise_level.lte => 25)
  scope :by_performance, desc(:performance_score)
  scope :by_price_asc, asc(:amazon_price)
  scope :by_price_desc, desc(:amazon_price)
  scope :by_creation_time_asc, asc(:created_at)
  scope :by_creation_time_desc, desc(:created_at)
  scope :with_amazon_price, ->(min, max) { between(:amazon_price => min.to_i..max.to_i) }

  #Parsing

  def parse_from_services(asin = self.amazon_asin, ym_link = self.yandex_market_link)
    begin
      if asin.present?
        self.amazon_asin ||= asin
        features = get_amazon_features(asin)
        if features.present? && features['ItemLookupResponse']['Items']['Request']['IsValid'] == 'True'
          amazon_features = features['ItemLookupResponse']['Items']['Item']
          self.fetch_amazon_features(amazon_features)
        end
      end

      if ym_link.present?
        self.yandex_market_link ||= ym_link
        yandex_features = get_yandex_features(ym_link)
        self.fetch_yandex_features(yandex_features) if yandex_features
      end
    rescue => e
      errors.add("Could not get data, reason: #{e}")
    end
  end

  def get_yandex_features(ym_link)
    if ym_link.present?
      all_features = {}

      doc = Nokogiri::HTML(open(ym_link))
      if doc.css('.b-properties').present?
        table = doc.css('.b-properties').first.css('tr')

        table.each do |tr|
          key = tr.css('th').css('span').text
          value = tr.css('td').text
          all_features[key] = value
        end
        return all_features
      end
    end
  end

  def get_amazon_features(asin = self.amazon_asin)
    if asin.present?
      request = Vacuum.new
      request.configure(
          aws_access_key_id:     AmazonApi::ACCESS_KEY,
          aws_secret_access_key: AmazonApi::SECRET_KEY,
          associate_tag:         AmazonApi::ASSOCIATES_ID
      )
      params = {  'IdType' => 'ASIN', 'ItemId' => asin, 'ResponseGroup'=> 'Images,ItemAttributes,Offers' }

      result = request.item_lookup(query: params)
      return result.to_h
    end
  end

  def fetch_amazon_features (item)
    self.remote_image_url = item['LargeImage']['URL']
    self.amazon_price = (item['OfferSummary']['LowestNewPrice']['Amount'].to_i/100).round(2)
    self.amazon_link = item['DetailPageURL']
    self.manufacturer = item['ItemAttributes']['Manufacturer']
    self.product_name = item['ItemAttributes']['Model']

    self.unpacked_weight = item['ItemAttributes']['ItemDimensions']['Weight']['__content__']
    self.packed_weight = item['ItemAttributes']['PackageDimensions']['Weight']['__content__']

    self.packed_height = item['ItemAttributes']['PackageDimensions']['Height']['__content__']
    self.packed_length = item['ItemAttributes']['PackageDimensions']['Length']['__content__']
    self.packed_width = item['ItemAttributes']['PackageDimensions']['Width']['__content__']

    self.unpacked_height = item['ItemAttributes']['ItemDimensions']['Height']['__content__']
    self.unpacked_length = item['ItemAttributes']['ItemDimensions']['Length']['__content__']
    self.unpacked_width = item['ItemAttributes']['ItemDimensions']['Width']['__content__']

    weight_to_kg(item['ItemAttributes']['ItemDimensions']['Weight']['Units'].to_s)
    dimensions_to_mm(item['ItemAttributes']['ItemDimensions']['Width']['Units'].to_s)
  end

  #Convertations

  def weight_to_kg(unit)
    self.unpacked_weight = convert_dimension(self.unpacked_weight, ConversionCoefficients::COEFFICIENTS[unit])
    self.packed_weight = convert_dimension(self.packed_weight, ConversionCoefficients::COEFFICIENTS[unit])
  end

  def dimensions_to_mm(unit)
    [:unpacked_height, :unpacked_width, :unpacked_length, :packed_height, :packed_width, :packed_length].each do |dimension|
      self[dimension] = convert_dimension(self[dimension], ConversionCoefficients::COEFFICIENTS[unit])
    end
  end

  def convert_dimension(dimension, coefficient)
    dimension *= coefficient if coefficient.present?

    return dimension.to_i == dimension ? dimension.to_i : dimension.round(2)
  end


# For using in inheritors

  def self.values_min_max(field_symbol, values_hash)
    all_values = self.distinct(field_symbol)
    all_values.compact!
    if all_values.length > 1
      values_hash[field_symbol] = {}

      values_hash[field_symbol][:min] = all_values.min
      values_hash[field_symbol][:max] = all_values.max
    end
  end

  def self.standard_filter_values
    values = {}
    self.values_min_max(:amazon_price, values)

    temp_values = self.distinct(:manufacturer)
    temp_values.reject! { |v| v.blank? }
    # If there is only one option, the choice is an illusion
    if temp_values.present? && temp_values.length > 1
      values[:manufacturer] = temp_values.sort_by! { |e| e.to_s.downcase }
    end

    return values
  end

  def value_from_dimension_string(dimension_string)
    value = 0
    value = dimension_string.split(' ').first.to_i if dimension_string.present?
    return value
  end

end
