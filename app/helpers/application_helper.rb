module ApplicationHelper
  include ActionView::Helpers::UrlHelper

  def hardware_title(hardware)
    return "#{hardware.manufacturer} #{hardware.product_name}"
  end

  def hardware_standard_features(hardware)
    features = t(hardware.hackintosh_recommended.to_s, scope: 'hardware.hackintosh_recommended')
    features << (', ' + t('hardware.noise_level', noise_level: hardware.noise_level)) unless hardware.noise_level.zero?
    features << (', ' + t('hardware.power_consumption', power_consumption: hardware.power_consumption)) unless hardware.power_consumption.zero?
  end

  def block_to_partial(partial_name, options = {}, &block)
    options.merge!(:body => capture(&block))
    render(:partial => partial_name, :locals => options)
  end

  def add_hardware_row(hardware_name, address, &block)
    content_tag(:div, class: 'row') do
      (content_tag(:div, class: 'col-md-2') do
        content_tag(:a, href: address, class: 'btn btn-primary btn-add') do
          content_tag(:span, ' ', class: 'glyphicon glyphicon-plus pull-left') +
          content_tag(:span, hardware_name.html_safe)
        end
      end) +
      (content_tag(:div, class: 'col-md-7') do
        yield
      end)
    end
  end

  def verified_span(computer)
    if computer.published?
      content_tag(:span, class: 'label label-success') do
        'Verified - all hardware is compatible'
      end
    else
      content_tag(:span, class: 'label label-warning') do
        'Hardware compatibility is not guaranteed'
      end
    end
  end

  def pack_set_hardware(pack_set, new_lines = false)
    temp_string = ''
    if pack_set.present?
      if new_lines
        pack_set.packs.each do |pack|
          temp_string += content_tag(:br) do
            "#{pack.count}x #{hardware_title(pack.hardware)}"
          end
        end
      else
        pack_set.packs.each do |pack|
          temp_string += "#{pack.count}x #{hardware_title(pack.hardware)}"
        end
      end
    end
    return temp_string.html_safe
  end

  def back_link(computer)
    if computer.present?
      content_tag(:p) do
        if can?(:edit, computer)
          link_to t('back'), edit_computer_path(computer)
        else
          link_to t('back'), computer_path(computer)
        end
      end
    end
  end

  def no_hardware_message(hardware)
    if params[:computer].present?
      return content_tag('h2') do
        "No compatible #{hardware} match filter criteria"
      end
    else
      return content_tag('h2') do
        "No #{hardware} match filter criteria"
      end
    end
  end

  def admin_signed_in?
    return user_signed_in? && current_user.admin?
  end

  def url_for_order_scope(order_scope)
    excluding_scopes = [:by_price_asc, :by_price_desc, :by_creation_time_asc, :by_creation_time_desc, :by_popularity]
    return url_for params.except(*excluding_scopes).merge(order_scope => true)
  end

  def admin_actions(hardware)
    html_string = ''
    if can? :edit, hardware
      html_string += content_tag(:p, class: 'admin-actions') do
        (link_to 'Edit', [:edit, hardware]) + ' | ' +
            (link_to 'Destroy', hardware, method: :delete, data: { confirm: 'Are you sure?' })
      end
    end
    html_string.html_safe
  end

  def form_submit(record, model_name = nil)
    html_string
  end

end

