source 'https://rubygems.org'

gem 'rails', '4.0.0'
gem 'devise'
gem 'mongoid', '>=2.1', git: 'https://github.com/mongoid/mongoid.git'
gem 'mongodb'
gem 'carrierwave-mongoid', :require => 'carrierwave/mongoid'
gem 'bootstrap-sass-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'turbolinks'
gem 'jbuilder'
gem 'bson_ext'
gem 'haml-rails'
gem 'factory_girl_rails'
gem 'mini_magick'
gem 'simple_form'
gem 'ffaker'
gem 'state_machine'
gem 'has_scope'
gem 'kaminari'
gem 'whenever'
gem 'cancan'
gem 'vacuum'
gem 'nokogiri'
gem 'recaptcha', :require => 'recaptcha/rails'

group :development, :test do
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'capybara'
  gem 'guard-rspec'
  gem 'guard-spork'
  gem 'spork'
  gem 'database_cleaner'
  gem 'puma'
end

group :development do
  gem 'capistrano'
  gem 'capistrano3-unicorn'
  gem 'capistrano-rails',   '~> 1.1', require: false
  gem 'capistrano-bundler', '~> 1.1', require: false
  gem 'capistrano-rvm', require: false
  gem 'net-scp'
  gem 'annotate'
end

group :test do
  gem 'rspec-rails'
  gem 'mongoid-rspec'
  gem 'rspec-given'
  gem 'rspec-instafail'
  gem 'capybara-puma'
  gem 'capybara-webkit'
  gem 'selenium-webdriver'
  gem 'fakeweb'
end

group :assets do
  gem 'sass-rails', '~> 4.0.0'
  gem 'uglifier', '>= 1.3.0'
  gem 'coffee-rails', '~> 4.0.0'
end

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :production do
  gem 'unicorn'
end

# Use debugger
# gem 'debugger', group: [:development, :test]
